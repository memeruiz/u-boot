/*
 *
    hexiaohua@rigol.com

    2017.1.12
    LED control.
 */

#include <common.h>
#include <command.h>
#include "keyled.h"

//! configs
#define KEYBOARD_LED_GROUP_NUM          4
#define KEYBOARD_LED_NUM                15
#define KEYBOARD_LED_GROUP_LEDS         4

static unsigned int n_u32KeyLedStat = 0x0FFFF;

static unsigned char n_au8KeyLedGroupCmd[KEYBOARD_LED_GROUP_NUM] = {
	0x10,                  /**< 第一组  LED4~LED1 */
	0x20,                  /**< 第二组  LED8~LED5 */
	0x30,                  /**< 第三组  LED12~LED9 */
	0x40                   /**< 第三组  LED16~LED13 */
};

static unsigned char n_au8KeyLedKey2LedMap[KEYBOARD_LED_NUM] = {
	0x01,                  /**< LED1  <-> KEYBOARD_LED_CH4 */
	0x02,                  /**< LED2  <-> KEYBOARD_LED_MATH */
	0x04,                  /**< LED5  <-> KEYBOARD_LED_SOURCE2 */
	0x08,                  /**< LED3  <-> KEYBOARD_LED_CH2 */

	0x01,                  /**< LED4  <-> KEYBOARD_LED_SOURCE1 */
	0x02,                  /**< LED6  <-> KEYBOARD_LED_CH1 */
	0x04,                  /**< LED7  <-> KEYBOARD_LED_DG */
	0x08,                  /**< LED13  <-> KEYBOARD_LED_RUN */

	0x01,                  /**< LED14  <-> KEYBOARD_LED_STOP */
	0x02,                  /**< LED8 <-> KEYBOARD_LED_SINGLE */
	0x04,                  /**< LED9 <-> KEYBOARD_LED_DECODE */
	0x08,                  /**< LED10 <-> KEYBOARD_LED_REF */

	0x01,                  /**< LED11 <-> KEYBOARD_LED_CH3 */
	0x02,                  /**< LED12 <-> KEYBOARD_LED_FUNC */
	0x04,                  /**< LED15 <-> KEYBOARD_LED_TOUCH */
};

static unsigned char scan_key[] = {
	KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7,
	KEY_SINGLE, KEY_RUN, KEY_BACK, KEY_OFF};

int get_pressed_key(void)
{
	int frame_len = 16;
	int i = 0;
	char frame_data[20];
	char key_matrix[12][8];

	memset(key_matrix, 0, sizeof(key_matrix));
	memset(frame_data, 0, sizeof(frame_data));
	for (i = 0; i < frame_len; i++)
		frame_data[i] = uart_zynq_uart1_getc();

	if (frame_data[0] == 0xaa) {
		unsigned short row = 0;
		int j = 0;
		int ind = 0;

		for (i = 0; i < 8; i++) {
			if (i % 2 == 1) {
				row = frame_data[ind];
				row = (row << 4 ) | (frame_data[ind + 1] >> 4);
				ind++;
			} else {
				row = frame_data[ind] & 0xf;
				row = (row << 8) | frame_data[ind + 1];
				ind += 2;
			}

			for (j = 0; j < 12; j++) {
				key_matrix[j][i] = 0x1 & (row >> 11);
				row = row << 1;
			}
		}
#if 0
		for (i=0; i < 12; i++) {
			for (j = 0; j < 8; j++)
				printf("%d ", key_matrix[i][j]);
			printf("\n");
		}
#endif
		unsigned char keys = sizeof(scan_key) / sizeof(scan_key[0]);
		int k = 0;

		for (k = 0; k < keys; k++) {
			i = (scan_key[k] & 0xf0) >> 4;
			j = scan_key[k] & 0xf;
			if (key_matrix[i][j] == 0)
				return scan_key[k];
		}
	}

	return 0;
}

void turn_led_on(unsigned int u32Leds)
{
	unsigned char u8Cmd;
	unsigned char u8GroupStat;
	unsigned char u8Led;
	unsigned char i, j;

	for (i = 0; i < KEYBOARD_LED_NUM; i++) {
		if (((u32Leds>>i) & 0x01)) {
			j = i / KEYBOARD_LED_GROUP_LEDS;
			u8Cmd = n_au8KeyLedGroupCmd[j];                                     /**< 获取组别 */
			u8GroupStat = n_u32KeyLedStat >> (j * KEYBOARD_LED_GROUP_LEDS);     /**< 得到当前该组键盘灯状态 */
			u8GroupStat &= 0x0F;
			u8Led = u8GroupStat & (~n_au8KeyLedKey2LedMap[i]);
			u8Led = u8GroupStat | n_au8KeyLedKey2LedMap[i];                     /**< 将对应位设置为 1 即点亮LED */
			u8Cmd |= u8Led;

			uart_zynq_uart1_putc(u8Cmd);

			n_u32KeyLedStat &= ~((unsigned int)0x0F << (j * KEYBOARD_LED_GROUP_LEDS));
			n_u32KeyLedStat |= (unsigned int)u8Led << (j * KEYBOARD_LED_GROUP_LEDS);
		}
	}
}

void turn_led_off(unsigned int u32Leds)
{
	unsigned int u8Cmd;
	unsigned int u8GroupStat;
	unsigned int u8Led;
	unsigned char i, j;

	for (i = 0; i < KEYBOARD_LED_NUM; i++) {
		if (((u32Leds >> i) & 0x01)) {
			j = i / KEYBOARD_LED_GROUP_LEDS;
			u8Cmd = n_au8KeyLedGroupCmd[j];                                     /**< 获取组别 */
			u8GroupStat = n_u32KeyLedStat >> (j * KEYBOARD_LED_GROUP_LEDS);     /**< 得到当前该组键盘灯状态 */
			u8GroupStat &= 0x0F;

			u8Led = u8GroupStat & (~n_au8KeyLedKey2LedMap[i]);                  /**< 将对应位清空为 0 即熄灭LED */
			u8Cmd |= u8Led;

			uart_zynq_uart1_putc(u8Cmd);

			n_u32KeyLedStat &= ~((unsigned int)0x0F << (j * KEYBOARD_LED_GROUP_LEDS));
			n_u32KeyLedStat |= (unsigned int)u8Led << (j * KEYBOARD_LED_GROUP_LEDS);
		}
	}
}

static int do_ledon(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int led = 0xffffffff;

	if (argc > 1) {
		led = simple_strtoul(argv[1], NULL, 10);
		if (led >= 0 && led < KEYBOARD_LED_NUM) {
			led = 1 << led;
		} else {
			printf("Invalid input.\n");
			led = 0;
		}
	}
	turn_led_on(led);

	return 0;
}


static int do_ledoff(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int led = 0xffffffff;

	if (argc > 1) {
		led = simple_strtoul(argv[1], NULL, 10);
		if (led >= 0 && led < KEYBOARD_LED_NUM) {
			led = 1 << led;
		} else {
			printf("Invalid input.\n");
			led=0;
		}
	}
	turn_led_off(led);

	return 0;
}

U_BOOT_CMD(
	ledon, 2, 0, do_ledon,
	"turn led on",
	"ledon [0~14],default is ALL"
);
U_BOOT_CMD(
	ledoff, 2, 0, do_ledoff,
	"turn led off",
	"ledoff [0~14],default is ALL"
);
