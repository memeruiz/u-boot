#include <common.h>
#include <command.h>

/* A few useful constants */
#define TAR_MAGIC               "ustar"        /* ustar and a null */
#define TAR_VERSION             "  "           /* Be compatable with GNU tar format */
#define FALSE                   0
#define TRUE                    1

static const int TAR_MAGIC_LEN = 6;
static const int TAR_VERSION_LEN = 2;
static const int TAR_BLOCK_SIZE = 512;

/* A nice enum with all the possible tar file content types */
enum TarFileType
{
	REGTYPE  = '0',            /* regular file */
	REGTYPE0 = '\0',           /* regular file (ancient bug compat)*/
	LNKTYPE  = '1',            /* hard link */
	SYMTYPE  = '2',            /* symbolic link */
	CHRTYPE  = '3',            /* character special */
	BLKTYPE  = '4',            /* block special */
	DIRTYPE  = '5',            /* directory */
	FIFOTYPE = '6',            /* FIFO special */
	CONTTYPE = '7',            /* reserved */
	GNULONGLINK = 'K',         /* GNU long (>100 chars) link name */
	GNULONGNAME = 'L',         /* GNU long (>100 chars) file name */
};

/* because gcc won't let me use 'static const int' */
enum {
	NAME_SIZE = 100
};

struct TarHeader
{
	/* byte offset */
	char name[NAME_SIZE];         /*   0-99 */
	char mode[8];                 /* 100-107 */
	char uid[8];                  /* 108-115 */
	char gid[8];                  /* 116-123 */
	char size[12];                /* 124-135 */
	char mtime[12];               /* 136-147 */
	char chksum[8];               /* 148-155 */
	char typeflag;                /* 156-156 */
	char linkname[NAME_SIZE];     /* 157-256 */
	char magic[6];                /* 257-262 */
	char version[2];              /* 263-264 */
	char uname[32];               /* 265-296 */
	char gname[32];               /* 297-328 */
	char devmajor[8];             /* 329-336 */
	char devminor[8];             /* 337-344 */
	char prefix[155];             /* 345-499 */
	char padding[12];             /* 500-512 (pad to exactly the TAR_BLOCK_SIZE) */
};
typedef struct TarHeader TarHeader;

struct TarInfo
{
	int              tarFd;          /* An open file descriptor for reading from the tarball */
	char *           name;           /* File name */
	mode_t           mode;           /* Unix mode, including device bits. */
	uid_t            uid;            /* Numeric UID */
	gid_t            gid;            /* Numeric GID */
	size_t           size;           /* Size of file */
	time_t           mtime;          /* Last-modified time */
	enum TarFileType type;           /* Regular, directory, link, etc. */
	char *           linkname;       /* Name for symbolic and hard links */
	long             devmajor;       /* Major number for special device */
	long             devminor;       /* Minor number for special device */
};
typedef struct TarInfo TarInfo;

static inline bool str2long(const char *p, uint *num)
{
	char *endptr;

	*num = simple_strtoul(p, &endptr, 16);
	return *p != '\0' && *endptr == '\0';
}

static int readTarHeader(struct TarHeader *rawHeader, struct TarInfo *header)
{
	int i;
	long chksum, sum=0;
	unsigned char *s = (unsigned char *)rawHeader;

	header->name  = rawHeader->name;

	/* Check for and relativify any absolute paths */
	if ( *(header->name) == '/' ) {
		static int alreadyWarned = FALSE;

		while (*(header->name) == '/')
			header->name++;

		if (alreadyWarned == FALSE) {
			printf("err in line:%d\n", __LINE__);
			alreadyWarned = TRUE;
		}
	}

	header->mode            = simple_strtol(rawHeader->mode, NULL, 8);
	header->uid             = simple_strtol(rawHeader->uid, NULL, 8);
	header->gid             = simple_strtol(rawHeader->gid, NULL, 8);
	header->size            = simple_strtol(rawHeader->size, NULL, 8);
	header->mtime           = simple_strtol(rawHeader->mtime, NULL, 8);
	chksum                  = simple_strtol(rawHeader->chksum, NULL, 8);
	header->type            = rawHeader->typeflag;
	header->linkname        = rawHeader->linkname;
	header->devmajor        = simple_strtol(rawHeader->devmajor, NULL, 8);
	header->devminor        = simple_strtol(rawHeader->devminor, NULL, 8);

	/* Check the checksum */
	for (i = sizeof(*rawHeader); i-- != 0;)
		sum += *s++;

	/* Remove the effects of the checksum field (replace
	 * with blanks for the purposes of the checksum) */
	s = (unsigned char*)(rawHeader->chksum);
	for (i = sizeof(rawHeader->chksum); i-- != 0;)
	    sum -= *s++;

	sum += ' ' * sizeof(rawHeader->chksum);
	if (sum == chksum)
		return (TRUE);

	return (FALSE);
}
static int do_tar(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret = 0,left;
	uint srcaddr = 0,dstaddr = 0,memaddr = 0;
	char *filename =NULL;
	char filesize[100];
	size_t readsize = 0;
	TarHeader rawHeader;
	TarInfo header;

	left = 1;
	while (argv[left])
		printf("%s:", argv[left++]);

	printf("\n");

	if (argc < 4) {
		printf("arg error\n");
		return 1;
	}

	if (!str2long(argv[1], &srcaddr)) {
		printf("'%s' is not a number\n", argv[1]);
		return 1;
	}

	if (!str2long(argv[2], &dstaddr)) {
		printf("'%s' is not a number\n", argv[2]);
		return 1;
	}

	filename = argv[3];
	if (srcaddr == 0 || dstaddr == 0 || filename == 0) {
		printf("arg error\n");
		return 1;
	}

	memaddr = srcaddr;
	while (1) {
		memcpy((char*)&rawHeader,(char*)memaddr,TAR_BLOCK_SIZE);

		/* Try to read the header */
		if (readTarHeader(&rawHeader, &header) == FALSE) {
			ret = 1;
			break;
		}

		if (header.type == REGTYPE) {
			size_t size = header.size;
			int mod = size % TAR_BLOCK_SIZE;

			if (strcmp(rawHeader.name,filename) == 0) {
				ret = 0;
				break;
			}

			if (mod != 0)
				readsize = size + (TAR_BLOCK_SIZE - mod);
			else
				readsize = size;
		} else {
			ret = 1;
			break;
		}
		memaddr += (TAR_BLOCK_SIZE + readsize);
	}

	if (ret == 0) {
		memcpy((char*)dstaddr,(char*)(memaddr + TAR_BLOCK_SIZE),header.size);
		sprintf(filesize,"0x%lx", (unsigned long)header.size);
		setenv("temp_file_size",filesize);
	} else {
		printf("File not found %s\n", filename);
	}

	return ret;
}

U_BOOT_CMD(
	tar, CONFIG_SYS_MAXARGS, 1, do_tar,
	"tar command by rigol",
	"srcaddr dstaddr filename"
);
