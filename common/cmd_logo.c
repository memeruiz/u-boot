/*
 *
	hexiaohua@rigol.com

	2016.7.4
		Load Zynq bit file and logo.
 */

#include <common.h>
#include <command.h>
#include <nand.h>
#include <net.h>
#include <malloc.h>
#include <video_fb.h>
#include "fpga.h"

extern void dpu_reset(void);
extern void dpu_clear_back(unsigned char col);

//Zynq bit location of Nand
#define ZYNQ_ADDR		0x4900000
#define ZYNQ_SIZE		0x3591FD

//Default logo location of Nand
#define LOGO_ADDR		0x4500000
#define MTD_SIZE		0x400000


#define SCR_WIDTH		1024
#define SCR_HEIGHT		600
//Buf of DDR3
#define LOAD_BUF		0x8000000

static int read_from_nand(int addr, int off, int size)
{
#if 0
	nand_info_t *nand 	= &nand_info[0];
	int			maxsize	= nand->size - size;

	return nand_read_skip_bad(nand, off, (size_t*)&size,
							 NULL, maxsize,
							 (unsigned char *)addr);
#endif
	char boot_cmd[256];

	memset(boot_cmd, 0, sizeof(boot_cmd));
	sprintf(boot_cmd, "nand read 0x%x 0x%x 0x%x", addr, off, size);

	return run_command(boot_cmd, 0);
}

static int do_loadzynq(cmd_tbl_t *cmdtp, int flag, int argc, const char *argv[])
{
	int zynq_addr = ZYNQ_ADDR;
	int zynq_size = ZYNQ_SIZE;

	if (argc > 1)
		zynq_addr = simple_strtoul(argv[1], NULL, 16);

	if (argc > 2)
		zynq_size = simple_strtoul(argv[2], NULL, 16);

	if (0 == read_from_nand(LOAD_BUF, zynq_addr, zynq_size) ) {
		int size = 1024;

		if (fpga_load(0, (const void*)LOAD_BUF, zynq_size, BIT_FULL) != 0) {
			printf("Load FPGA ERROR.%s, %d\r\n", __func__, __LINE__);
			return 1;
		}

		// clear uart fifo
		// Empty rx fifo
		while (tstc() && size > 0) {
			(void)getc();
			size--;
		}
		return 0;
	}
	return 1;
}

// 0-default logo, 1-user logo
static int check_logo(int addr)
{
	int logo_head[4] = { 0, 0, 0, 0 };

	if (0 == read_from_nand((int)&logo_head[0], addr, 8)) {
		if (logo_head[0] > 0 &&
		    logo_head[1] > 0 &&
		    logo_head[0] < SCR_WIDTH &&
		    logo_head[1] < SCR_HEIGHT)
			return 1;
	}

	return 0;
}

static int do_loadlogo(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int logo_addr = LOGO_ADDR;
	int mtd_size  = MTD_SIZE;
	int ram_addr  = DPU_BT_BASEADDR;
	int scr_size  = SCR_WIDTH * SCR_HEIGHT * 2  + 8;

	if (argc > 1)
		logo_addr = simple_strtoul(argv[1], NULL, 16);

	if (argc > 2)
		mtd_size  = simple_strtoul(argv[2], NULL, 16);

	if (argc > 3)
		ram_addr  = simple_strtoul(argv[3], NULL, 16);
#if 1
	// check user logo is set
	if (check_logo(logo_addr + mtd_size) == 1)
		logo_addr = logo_addr + mtd_size;
#endif
	if (0 == read_from_nand(LOAD_BUF, logo_addr, scr_size)) {
		int *p = (int *)LOAD_BUF;
		int width  = *p++;
		int height = *p++;

		if (width > 0 &&
		    height > 0 &&
		    width <= SCR_WIDTH &&
		    height <= SCR_HEIGHT) {
			unsigned short *pRam = (unsigned short*)ram_addr;
			unsigned short *logo = NULL;
			int i, j;
			int x, y;

			logo = (unsigned short*)p;
			x = (SCR_WIDTH - width) >> 1;
			y = ((SCR_HEIGHT - height) >> 1) - 8; // fixed the view misorder
			pRam = pRam + (y - 1) * SCR_WIDTH + (x - 1);
			for (i = 0; i < height; i++) {
				for (j = 0; j < width; j++)
					*(pRam+j) = *logo++;
				pRam += SCR_WIDTH;
			}

			dpu_reset();

			printf("Loading logo, x=%d, y=%d, width=%d, height=%d\n",
			        x, y, width, height);

			return 0;
		} else {
			printf("Invalid width and height: %d, %d\n",
			       width, height);
		}
	} else {
		printf("logo reading error: %d\n", logo_addr);
	}

	return 1;
}


static int do_dpu(cmd_tbl_t *cmdtp, int flag, int argc, const char *argv[])
{
	// pGD = (GraphicDevice*)video_hw_init();
	// video_fb_address = (void *) VIDEO_FB_ADRS;
	// video_console_address = video_fb_address;
	dpu_reset();

	return 0;
}

U_BOOT_CMD(
	help, CONFIG_SYS_MAXARGS, 1, do_help,
	dpu, 1, 0, do_dpu,
	"Init DPU", ""
);

U_BOOT_CMD(
	loadzynq, 3, 0, do_loadzynq,
	"load zynq bit",
	"loadzynq [addr] [size]"
);

U_BOOT_CMD(
	loadlogo, 4, 0, do_loadlogo,
	"load logo",
	"loadlogo [addr] [mtdsize] [video ram addr]"
);
