#include <common.h>
#include <command.h>

#define MAX_SH_SIZE             0x100000

static char Shell_CMD_Buf[CONFIG_SYS_CBSIZE];

static inline bool hexstr2long(const char *p, uint *num)
{
	char *endptr;

	*num = simple_strtoul(p, &endptr, 16);

	return *p != '\0' && *endptr == '\0';
}

static int do_execshell(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret = 1;
	uint srcaddr = 0, index = 0,num = 0;
	char *memaddr = NULL;

	if (argc < 2)
		return 1;

	if (!hexstr2long(argv[1], &srcaddr)) {
		printf("'%s' is not a number\n", argv[1]);
		return 1;
	}

	if (srcaddr == 0) {
		printf("arg error\n");
		return 1;
	}

	memaddr = (char *)srcaddr;
	memset(Shell_CMD_Buf,0,CONFIG_SYS_CBSIZE);
	while (memaddr[index] != '\0' &&
	       index < MAX_SH_SIZE) {
		for (num = 0; num < CONFIG_SYS_CBSIZE; num++) {
			if (memaddr[index] == '\\') {
				do
					index++;
				while(memaddr[index] != '\n' &&
				      memaddr[index] != '\0'); // find "\     \n"
				index++;
				if(memaddr[index] != '\n' &&
				   memaddr[index] != '\0') {
					Shell_CMD_Buf[num] = memaddr[index];
					index++;
				}
			} else if(memaddr[index] == '\n') {
				if (num == 0) {
					index++;
					if(memaddr[index] != '\n' &&
					   memaddr[index] != '\0') {
						Shell_CMD_Buf[num] = memaddr[index];
						index++;
					}
				} else {
					break;
				}
			} else if(memaddr[index] != '\n' &&
			          memaddr[index] != '\0') {
				Shell_CMD_Buf[num] = memaddr[index];
				index++;
			}
		}

		if(Shell_CMD_Buf[0] != '#' &&
		   Shell_CMD_Buf[0] != '\n' &&
		   Shell_CMD_Buf[0] != '\0') {
			if(run_command(Shell_CMD_Buf,0) != 0) {
				printf ("Stop executing for error\n");
				return 1;
			} else {
				memset(Shell_CMD_Buf,0,CONFIG_SYS_CBSIZE);
				index++;
			}
		} else {
			//check ##end##
			if(Shell_CMD_Buf[1] == '#' &&
			   Shell_CMD_Buf[2] == 'e' &&
			   Shell_CMD_Buf[3] == 'n' &&
			   Shell_CMD_Buf[4] == 'd' &&
			   Shell_CMD_Buf[5] == '#' &&
			   Shell_CMD_Buf[6] == '#') {
				ret = 0;
				//sh end
				break;
			} else {
				//sh 注释
				memset(Shell_CMD_Buf,0,CONFIG_SYS_CBSIZE);
				index++;
			}
		}
	}

	return ret;
}

U_BOOT_CMD(
	exec, CONFIG_SYS_MAXARGS, 1, do_execshell,
	"exec memaddr, return 0 on success, or != 0 on error by rigol",
	"memaddr"
);
