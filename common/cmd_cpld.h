#ifndef _CMD_CPLD_
#define _CMD_CPLD_

/*************************SPI Paramater*****************************/
#define SPI_CS_CPLD             0
#define SPI_CS_DEV              1
#define SPI_BUS                 0
#define SPI_SPEED               10000000

/*************************SPI COMMAND*******************************/
#define CPLD_VERSION            0x0
#define CFG_RST                 0x01
#define CFG_BEEPER              0x02

#define CFG_SPI_DEV             0x04
#define CFG_BACKLIGHT           0x05
#define CFG_POWER               0x06

#define CFG_QSPI_CS             0x08

#define SP_VERSION              0x0b

/*************************Beeper and backlight*******************************/
#define BEEPER_ONCE             0x03
#define BEEPER_INTERVAL         0x02
#define BEEPER_CONTINUE         0x01
#define BEEPER_OFF              0x0
#define BACKLIGHT_ON(V)         (0x80 | V)
#define BACKLIGHT_OFF           (0)
#define POWER_RESTART           (0x80)

/*************************Device reset*******************************/
#define RST_ADC                 0x0001
#define RST_SADC                0x0002
#define RST_PLL_5G              0x0004

/*************************Device select*******************************/
#define DEV_ADC                 0x0001
#define DEV_SADC                0x0002
#define DEV_PLL_5G              0x0004
#define DEV_PLL1_4_4G           0x0008
#define DEV_AFE_SRL             0x0010
#define DEV_VGA_CH4             0x0020
#define DEV_VGA_CH3             0x0040
#define DEV_VGA_CH2             0x0080
#define DEV_VGA_CH1             0x0100

#endif
