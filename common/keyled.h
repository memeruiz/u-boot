#ifndef __KEY_LED__
#define __KEY_LED__

//! led
#define LED_CH4                 (1<<0)
#define LED_MATH                (1<<1)
#define LED_SOURCE2             (1<<2)
#define LED_CH2                 (1<<3)
#define LED_SOURCE1             (1<<4)
#define LED_CH1                 (1<<5)
#define LED_DIGITAL             (1<<6)
#define LED_RUN                 (1<<7)
#define LED_STOP                (1<<8)
#define LED_SINGLE              (1<<9)
#define LED_DECODE              (1<<10)
#define LED_REF                 (1<<11)
#define LED_CH3                 (1<<12)
#define LED_FUNC                (1<<13)
#define LED_TOUCH               (1<<14)
#define LED_ALL                 (0xffffffff)

#define KEY_OFF                 0X64
#define KEY_F1                  0X53
#define KEY_F2                  0X52
#define KEY_F3                  0X51
#define KEY_F4                  0X50
#define KEY_F5                  0X55
#define KEY_F6                  0X56
#define KEY_F7                  0X97
#define KEY_BACK                0X57

#define KEY_HELP                0X86

#define KEY_RUN                 0X80
#define KEY_AUTO                0X82
#define KEY_DEFAULT             0X73
#define KEY_SINGLE              0X85
#define KEY_CLEAR               0X84

#define GOLD_FINGER             0
#define ONBOARD_QSPI            1

enum BOOT_MSG
{
	MSG_FOR_START = 0,
	MSG_FOR_FIREWARE,
	MSG_FOR_DEFAULT,
	MSG_FOR_RESTART,
	MSG_FOR_CONTINUE,
	MSG_FOR_FAIL,
	MSG_FOR_BURN,
	MSG_FOR_OK,
	MSG_FOR_ALL
};

extern void uart_zynq_uart1_putc(unsigned char c);
extern int uart_zynq_uart1_tstc(void);
extern int uart_zynq_uart1_getc(void);
extern int uart_zynq_uart1_rst(void);
extern void video_position_cursor(unsigned col, unsigned row);
extern void video_puts(const char*);
void turn_led_off(unsigned int u32Leds);
void turn_led_on(unsigned int u32Leds);
void show_boot_msg(int msg);
void show_msg(const char *msg);
int get_pressed_key( void );
int detect_gold_finger(void);
int set_boot_from(int from);
void beeper(int cmd);
#endif
