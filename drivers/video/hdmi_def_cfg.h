/*
 * hdmi_dri.h
 *
 *  Created on: 2016年3月10日
 *      Author: sn02388
 */

#ifndef HDMI_DEF_CFG_H_
#define HDMI_DEF_CFG_H_

//#include"stdbool.h"

//预定义屏幕分辨率(适应不同长宽比的屏幕)
#define HDMI_1024_768_60		0        //4:3屏幕    //需要65M
#define HDMI_1280_960_60		1                     //需要108M
#define HDMI_1152_864_75		2                     //需要108M
#define HDMI_800_600_60			3                     //需要40M

#define HDMI_1280_1024_60		4        //5:4屏幕    //需要108M
#define HDMI_640_480_60			5
#define HDMI_640_480_75			6
#define HDMI_720_576_50			7

#define HDMI_1280_720_60		8        //16:9屏幕   //需要74.25M
#define HDMI_1920_1080_60		9                     //需要148.5M

#define HDMI_720_480_60			10       //3:2屏幕
#define HDMI_1360_768_60		11       //近似1.77比例 //16:10屏幕分辨率未找到

#define HDMI_OPEN			1        //使能控制
#define HDMI_CLOSE			0


#define HDMI_16_10			0


//hdmi寄存器地址
#define HDMI_PIC_BASEADDR		0x43c00000     //该地址为GP0总线接口的基地址

#define HDMI_PIC_BASEADDR_CFG		16             //偏移量不能与dpu的config_reg 偏移量重复
#define HDMI_BASE_ADDR_MASK			0xFFFFFFFF
#define HDMI_H_NUM_CFG			17
#define HDMI_H_COUTN_MASK			0x00000FFF
#define HDMI_H_SYNC_MASK			0x000FF000
#define HDMI_PIC_H_OFFSET_MASK			0xFFF00000

#define HDMI_H_EN_NUM_CFG		18
#define HDMI_H_EN_MIN_MASK			0x00000FFF
#define HDMI_H_EN_MAX_MASK			0x00FFF000
#define HDMI_V_NUM_CFG			20
#define HDMI_V_COUNT_MASK			0x00000FFF
#define HDMI_V_SYNC_MASK			0x0000F000
#define HDMI_PIC_V_OFFSET_MASK			0x0FFF0000
#define HDMI_V_EN_NUM_CFG		21
#define HDMI_V_EN_MIN_MASK			0x00000FFF
#define HDMI_V_EN_MAX_MASK			0x00FFF000
#define HDMI_EN_MASK				0x40000000

#define HDMI_RST_CFG			22
#define HDMI_RST_MASK				0x80000000
#define HDMI_CLK_CFG0			23
#define HDMI_CLK_CFG1			24
#define HDMI_CLK_CFG2			25
#define HDMI_CLK_CFG3			26
#define HDMI_CLK_CFG4			27
#define HDMI_CLK_CFG5			28
#define HDMI_CLK_CFGEN			29
#define HDMI_CLK_CFGEN_MASK			0x80000000
#define HDMI_PIC_CFG			30
#define HDMI_PIC_HEIGHT_MASK			0x00000fff
#define HDMI_PIC_WIDTH_MASK			0x00fff000
#define HDMI_DATA_TYPE_MASK			0x03000000
#define HDMI_SYNC_POLARITY			0x0c000000

#define HDMI_BASE_ADDR_SET(PIC_ADDR) \
	((PIC_ADDR) & HDMI_BASE_ADDR_MASK)

#define HDMI_H_NUM_SET(H, V, W) \
	((((H) << 0) & HDMI_H_COUTN_MASK) | \
	 (((V) << 12) & HDMI_H_SYNC_MASK) | \
	 (((W) << 20) & HDMI_PIC_H_OFFSET_MASK))

#define HDMI_H_EN_NUM_SET(H_MIN, H_MAX)	\
	((((H_MIN) << 0) & HDMI_H_EN_MIN_MASK) | \
	 (((H_MAX) << 12) & HDMI_H_EN_MAX_MASK))

#define HDMI_V_NUM_SET(H, V, W) \
	((((H) << 0) & HDMI_V_COUNT_MASK) | \
	 (((V) << 12) & HDMI_V_SYNC_MASK ) | \
	 (((W) << 16) & HDMI_PIC_V_OFFSET_MASK))

#define HDMI_V_EN_NUM_SET(V_MIN, V_MAX, EN) \
	((((V_MIN) << 0) & HDMI_V_EN_MIN_MASK) | \
	 (((V_MAX) << 12) & HDMI_V_EN_MAX_MASK) | \
	 (((EN) << 30) & HDMI_EN_MASK))

#define HDMI_RST_SET(RST) \
	(((RST) << 31) & HDMI_RST_MASK)

#define HDMI_CLK_CFG_SET(DATA) (DATA)
#define HDMI_CLK_CFGEN_SET(HDMICLKCFGEN) \
	(((HDMICLKCFGEN) << 31) & HDMI_CLK_CFGEN_MASK)
#define HDMI_PIC_SET(HEIGHT, WIDTH, DATATYPE, SYNCPOLARITY) \
	((((HEIGHT) << 0) & HDMI_PIC_HEIGHT_MASK) | \
	 (((WIDTH) << 12) & HDMI_PIC_WIDTH_MASK) | \
	 (((DATATYPE) << 24) & HDMI_DATA_TYPE_MASK) | \
	 (((SYNCPOLARITY) << 26) & HDMI_SYNC_POLARITY))

#define HDMI_SET_CFG(OFF) (HDMI_PIC_BASEADDR | ((OFF) << 2))

#define HDMIDATATYPE_R8G8B8		0x0                 // 24位色32bit
#define HDMIDATATYPE_R8G8B8_32		0x1                 // 24位色24BIT
#define HDMIDATATYPE_R5G6B5		0x2                 // 16位色
#define HDMIDATATYPE_INDEX8		0x3                 // 8位色

/*************************************************************************************************************/

//以下为与EDID解析有关的数据

//內建时序I 定义（低字节）
#define R720_400_70		7
#define R720_400_88		6
#define R640_480_60		5
#define R640_480_67		4
#define R640_480_72		3
#define R640_480_75		2
#define R800_600_56		1
#define R800_600_60		0

//內建时序II 定义(高字节)
#define R800_600_72		7
#define R800_600_75		6
#define R832_624_75		5
#define R1024_768_87		4
#define R1024_768_60		3
#define R1024_768_70		2
#define R1024_768_75		1
#define R1280_1024_75		0

//內建时序III 定义
//(第1字节)
#define R640_350_85		7
#define R640_400_85		6
#define R720_400_85		5
#define R640_480_85		4
#define R848_480_85		3
#define R800_600_85		2
#define R1024_768_85		1
#define R1152_864_85		0

//(第2字节)
#define R1280_768_60_RB		7  //reduced blanking
#define R1280_768_60		6
#define R1280_768_75		5
#define R1280_768_85		4
#define R1280_960_60		3
#define R1280_960_85		2
#define R1280_1024_60		1
#define R1280_1024_85		0

//(第3字节)
#define R1360_768_60		7
#define R1440_900_60_RB		6
#define R1440_900_60		5
#define R1440_900_75		4
#define R1440_900_85		3
#define R1400_1050_60_RB	2
#define R1400_1050_60		1
#define R1400_1050_75		0

//(第4字节)
#define R1400_1050_85		7
#define R1680_1050_60_RB	6
#define R1680_1050_60		5
#define R1680_1050_75		4
#define R1680_1050_85		3
#define R1600_1200_60		2
#define R1600_1200_65		1
#define R1600_1200_70		0

//(第5字节)
#define R1600_1200_75		7
#define R1600_1200_85		6
#define R1792_1344_60		5
#define R1792_1344_75		4
#define R1856_1392_60		3
#define R1856_1392_75		2
#define R1920_1200_60_RB	1
#define R1920_1200_60		0

//(第一字节)
#define R1920_1200_75		7
#define R1920_1200_85		6
#define R1920_1440_60		5
#define R1920_1440_75		4

//长宽比宏定义
#define RADIO_16_9		0
#define RADIO_16_10		1
#define RADIO_4_3		2
#define RADIO_5_4		3


//分辨率大小定义
//定义标准时序结构体
typedef struct {
	 unsigned short hnum;   //行像素值
	 unsigned short vnum;   //列像素值
	 unsigned short freq;   //刷新率
	 unsigned char radio;   //比例关系  ,高四位：第四位 =长：宽
} Standard_Time;

//定义标准时序结构体
typedef struct {
	 Standard_Time Std_Tim[8];     //最多8个标准时序
	 unsigned char Stand_Tim_Num;  //存储标准时序个数
} Standard_Tim;


//定义详细时序结构体
typedef struct {
	 unsigned short Pixel_clk;     //像素速率 ，单位 10k
	 unsigned short Haddressable;  //行像素值
	 unsigned short Hblacking;     //行无效像素值
	 unsigned short HFrontPorch;   //
	 unsigned short HSyncPorch;    //

	 unsigned short Vaddressable;  //列像素值
	 unsigned short Vblacking;     //无效列
	 unsigned short VFrontPorch;   //
	 unsigned short VSyncPorch;    //

	 unsigned char radio;         //比例关系  ,高四位：第四位 =长：宽
	 unsigned char InterfaceType;  //接口特征

} Detaild_Time;

//定义标准时序结构体
typedef struct {
	Detaild_Time Detaild_Tim[4];     //最多4个详细时序存储空间基本够用
	unsigned char Detaild_Tim_Num;     //存储详细时序个数
} Detaild_Tim;


//定义参数范围结构体
typedef struct {
	unsigned short Min_v;
	unsigned short Max_v;
	unsigned short Min_h;
	unsigned short Max_h;
	unsigned short Max_pixel_clk;

} Tim_Lit;


//LCD厂商等参数定义
typedef struct {
	unsigned char Manufac_ID[3];
	unsigned short Product_ID;
	unsigned long Product_Serial;
	unsigned char Week;
	unsigned short Year;
} Manufac_Inf;

//EDID版本结构体
typedef struct {
	unsigned char version;
	unsigned char revision;
} EDID_Version;


//以上为与EDID解析有关的定义
/*************************************************************************************************************/

//hdmi配置参数
typedef struct {
	unsigned long hdmi_pic_baseaddr;
	unsigned long hdmi_en;
	unsigned long hdmi_h_num;
	unsigned long hdmi_h_sync_num;
	unsigned long hdmi_h_en_min;
	unsigned long hdmi_h_en_max;
	unsigned long hdmi_h_pic_offset;

	unsigned long hdmi_v_num;
	unsigned long hdmi_v_sync_num;
	unsigned long hdmi_v_en_min;
	unsigned long hdmi_v_en_max;
	unsigned long hdmi_v_pic_offset;

	unsigned long hdmi_pic_height;
	unsigned long hdmi_pic_width;
	unsigned char hdmi_pic_data_type;
	unsigned char hdmi_sync_polarity;
} ST_HDMI_D;


////////////////////////////////////函数实现///////////////////////////////////'

void devhdmiRegSet(unsigned long *pu32RegAddr, unsigned long u32RegData);

void devHDMI_RESET(void);

//hdmi参数手动设置
void devHDMI_SET_NUM(unsigned long hdmi_en, unsigned long hdmidisaddr,
                     unsigned long h_count, unsigned long hsync_num,
                     unsigned long h_en_min, unsigned long h_en_max,
                     unsigned long v_count, unsigned long vsync_num,
                     unsigned long v_en_min, unsigned long v_en_max,
                     unsigned long pic_h_offset, unsigned long pic_v_offset,
                     unsigned long hdmi_pic_height, unsigned long hdmi_pic_width,
                     unsigned char hdmi_pic_data_type, unsigned char sync_polarity);

void devHDMI_SET_Dis_Baseaddr(unsigned long baseaddr);

int devDPUHDMI_SET_DEF(unsigned char hdmi_definition, unsigned long baseaddr,
                       unsigned long width, unsigned long height,
                       unsigned char datatype, unsigned long hdmi_en);

void devHdmiRegSet(unsigned long *pu32RegAddr,        // 寄存器地址
                   unsigned long u32RegData);         // 寄存器数据

void devHDMI_SET_CFG(ST_HDMI_D*  hdmi_cfg);

void devHDMI_CLK_CFG(unsigned char hdmi_definition);

int devDPUHDMI_SET_DEF(unsigned char hdmi_definition, unsigned long baseaddr,
                       unsigned long width, unsigned long height,
                       unsigned char datatype, unsigned long hdmi_en);


int SetHDMI_perfect_res(unsigned char dattemp[], unsigned long baseaddr,
                        unsigned long width, unsigned long height,
                        unsigned char datatype, unsigned char en_ctl);

void Edidfunc_test(unsigned char dattemp[]);

int EDID_Data_vld(unsigned char dattemp[]);

void Judge_ManufacturerID(unsigned char dattemp[], Manufac_Inf *manufac_inf);
void Judge_EDID_Version(unsigned char dattemp[], EDID_Version *edid_version);
void Judge_Established_Timing(unsigned char dattemp[], unsigned char *Established_Timing);
void Judge_Standard_Timing(unsigned char dattemp[], Standard_Tim *Res_Standrad);
void Judge_Preferred_Timing(unsigned char dattemp[], Detaild_Time *Res_PreferredTime);

void Juding_DisRange_limit(unsigned char dattemp[],      //EDID数组
                           Tim_Lit* tim_lit,             //设置范围 ,一定有
                           Standard_Tim* Res_Standrad,   //标准时序，可能存在
                           unsigned char* Established_Tim_III,  //需要5个字节接收
                           unsigned char* ProName);      //product name,一定有

int extension_vld(unsigned char dattemp[]);
void Judge_CEA_Extension_Version(unsigned char dattemp[], EDID_Version *extension_ver);
void Exten_Detail_Tim(unsigned char dattemp[], Detaild_Tim *Res_extendTim);
unsigned char Search_Standard_Tim(Standard_Tim *Res_Standrad_Tim, unsigned short hnum, unsigned short vnum);
unsigned char Search_ExtendTim_Tim(unsigned char dattemp[] ,Detaild_Tim* Res_extendTim,\
unsigned short hnum, unsigned short vnum);
void Convert_dataildtim2param(Detaild_Time *Res_extendTime, ST_HDMI_D *Hdmi_param);

#endif /* HDMI_DEF_CFG_H_ */
