#include <common.h>
#include <asm/io.h>
#include <os.h>
#include <video_fb.h>
#include "zynq_fb.h"
#include "hdmi_def_cfg.h"

/* Layer defination in video_fb.h   */
ST_DPU_CFG gstDPUConfig;
static GraphicDevice ctfb;
enum {
	DPU_Layer_Back,
	DPU_Layer_Wave,
	DPU_Layer_Eye,
	DPU_Layer_Fore,
	DPU_Layer_Comp,
	DPU_Layer_All,
};

extern void lcd_backlight_on(void);
void clearLayer(int layer, unsigned char col);

/*********devDPU********************/
void devDPURegSet(unsigned long *pu32RegAddr,        // 寄存器地址
                  unsigned long  u32RegData) {       // 寄存器数据
	*pu32RegAddr = u32RegData;
}

void devDPUCfgLayerSet(unsigned char u8LayerNumber,
                       unsigned short u16ScrnHeight,
                       unsigned short u16ScrnWidth) {
	devDPURegSet((unsigned long*)(DPU_REG_CFG(DPU_LAYER_CFG_REG)),
	             DPU_LAYER_CFG_SET(u8LayerNumber, u16ScrnHeight, u16ScrnWidth));
}

void devDPULCDReset(void)
{
	devDPURegSet((unsigned long*)(DPU_REG_CFG(DPU_LCD_RST_REG)), DPU_LCD_RST_MASK);
	devDPURegSet((unsigned long*)(DPU_REG_CFG(DPU_LCD_RST_REG)), 0);
}

unsigned int drvDPUVersion(void)
{
	unsigned int u32Temp;

	u32Temp = *(unsigned int*)(DPU_VERSION_REG);

	return u32Temp;
}

bool devDPULayerCfgSet(ST_LAYER_CFG* pstLayerConfig)
{
	unsigned char i,j;

	for(i = 0; i < DPU_LAYER_NUM; i++) {
		for(j = 0; j <= DPU_LAYER_CFG_NUM; j++)
			*((unsigned long *)DPU_LAYER_CFG(i, j)) = *((unsigned long*)(pstLayerConfig + i) + j);
	}

	//if ((DPU_LAYER_NUM != 15) || (DPU_LAYER_CFG_NUM != 7))
	{
		*((unsigned long*)DPU_LAYER_CFG(15, 7)) = 0;
	}

	devDPURegSet((unsigned long*)(DPU_REG_CFG(DPU_LAYER_PAUSE_REG)), 0);

	return (true);
}

unsigned short drvDPUInit(void)
{
	memset(&gstDPUConfig.stLayerConfig[0], 0 , sizeof(gstDPUConfig.stLayerConfig));

	gstDPUConfig.u8LayerNumber = DPU_Layer_All;             // 图层数量
	//L1
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].bLayerCombinable     = true;                  // 第一层，必须合并
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u3LayerWriteMode     = DPU_LAYER_EXTER_COMB;  // 图层间合并
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u3LayerColorType     = DPU_LAYER_R5G6B5;      // 16位色
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u2LayerCombMode      = DPU_LAYER_OVERLAY;     // 第一层，必须为覆盖
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u3LayerType          = DPU_LAYER_PIC;         // 第一层，必须为图片层
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u9LayerHeight        = DPU_SCRN_HEIGHT;       // 第一层，必须为屏幕高度
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u10LayerWidth        = DPU_SCRN_WIDTH;        // 第一层，必须为屏幕宽度
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u8LayerAlpha         = 255;                   // 配置透明度，0全透明，255不透明
	gstDPUConfig.stLayerConfig[DPU_Layer_Back].u32LayerBaseaddr     = DPU_BT_BASEADDR;       // 图层存储基地址，4字节对齐

#if 1
	//Wave
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].bLayerCombinable     = false;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u3LayerWriteMode     = DPU_LAYER_EXTER_COMB;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u3LayerColorType     = DPU_LAYER_R8G8B8;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u2LayerCombMode      = DPU_LAYER_BOTH;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u3LayerType          = DPU_LAYER_PIC;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u9LayerHeight        = DPU_LAYER_WAVE_HEIGHT;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u10LayerWidth        = DPU_LAYER_WAVE_WIDTH;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u9LayerPosy          = 56;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u10LayerPosx         = 23;
	gstDPUConfig.stLayerConfig[DPU_Layer_Wave].u32LayerBaseaddr     = DPU_LW_BASEADDR;

	//eye
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].bLayerCombinable      = false;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u3LayerWriteMode      = DPU_LAYER_EXTER_COMB;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u3LayerColorType      = DPU_LAYER_R8G8B8;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u2LayerCombMode       = DPU_LAYER_BOTH;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u3LayerType           = DPU_LAYER_PIC;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u9LayerHeight         = DPU_LAYER_WAVE_HEIGHT;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u10LayerWidth         = DPU_LAYER_WAVE_WIDTH;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u9LayerPosy           = 56;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u10LayerPosx          = 23;
	gstDPUConfig.stLayerConfig[DPU_Layer_Eye].u32LayerBaseaddr      = DPU_LE_BASEADDR;

	//Fore layer
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].bLayerCombinable     = false;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u3LayerWriteMode     = DPU_LAYER_EXTER_COMB;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u3LayerColorType     = DPU_LAYER_R5G6B5;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u2LayerCombMode      = DPU_LAYER_ALPHA;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u3LayerType          = DPU_LAYER_PIC;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u9LayerHeight        = DPU_SCRN_HEIGHT;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u10LayerWidth        = DPU_SCRN_WIDTH;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u8LayerAlpha         = 64;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u9LayerPosy          = 0;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u10LayerPosx         = 0;
	gstDPUConfig.stLayerConfig[DPU_Layer_Fore].u32LayerBaseaddr     = DPU_LF_BASEADDR;
#endif

	// 合并层
	// 合并层宽度必须和背景层大小一致
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].bLayerCombinable     = true;                  // 合并
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u3LayerWriteMode     = DPU_LAYER_EXTER_WR;    // 合并层写入
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u3LayerColorType     = DPU_LAYER_R5G6B5;      // 位色
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u2LayerCombMode      = DPU_LAYER_OVERLAY;     // 透明
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u3LayerType          = DPU_LAYER_COMB;        // 图片
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u9LayerHeight        = DPU_SCRN_HEIGHT;       // 高度
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u10LayerWidth        = DPU_SCRN_WIDTH;        // 宽度

	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u8LayerAlpha         = 255;                   // 配置透明度，0全透明，255不透明
	gstDPUConfig.stLayerConfig[DPU_Layer_Comp].u32LayerBaseaddr     = DPU_LC_BASEADDR;       // 图层存储基地址，4字节对齐

	//**************************************************************************
	//配置写入
	//**************************************************************************
	devDPUCfgLayerSet(gstDPUConfig.u8LayerNumber, DPU_SCRN_HEIGHT, DPU_SCRN_WIDTH);
	devDPULayerCfgSet(&gstDPUConfig.stLayerConfig[0]);                      // 写入图层配置
	devDPULCDReset();

	return 0;
}


void clearLayer(int layer, unsigned char col)
{
	int addr  = gstDPUConfig.stLayerConfig[layer].u32LayerBaseaddr;
	int h     = gstDPUConfig.stLayerConfig[layer].u9LayerHeight;
	int w     = gstDPUConfig.stLayerConfig[layer].u10LayerWidth;
	int depth = 2;
	unsigned char *pMem = (unsigned char *)addr;

	if (gstDPUConfig.stLayerConfig[layer].u3LayerColorType == DPU_LAYER_R8G8B8)
		depth = 4;

	memset(pMem, col, h * w * depth);
}


void enableHDMI(int en)
{
	extern void set_hdmi(unsigned long baseaddr, unsigned char datatype, int open);

	devHDMI_CLK_CFG(HDMI_720_480_60);
	devDPUHDMI_SET_DEF(HDMI_720_480_60, DPU_LC_BASEADDR, 1024, 600, HDMIDATATYPE_R5G6B5, en);
}

void dpu_clear_back(unsigned char col)
{
	clearLayer(DPU_Layer_Back, col);
}

void dpu_reset(void)
{
	//enableHDMI(0);
	drvDPUInit();
	mdelay(200); //delay for DPU draw the first picture
	lcd_backlight_on();
}

void* video_hw_init(void)
{
	GraphicDevice *gdev = &ctfb;
	memset(gdev, 0, sizeof(ctfb));
	//dpu_reset();

	gdev->winSizeX          = DPU_SCRN_WIDTH;
	gdev->winSizeY          = DPU_SCRN_HEIGHT;

	gdev->plnSizeX          = DPU_SCRN_WIDTH;
	gdev->plnSizeY          = DPU_SCRN_HEIGHT;

	gdev->gdfBytesPP        = 2;
	gdev->gdfIndex          = GDF_16BIT_565RGB;

	gdev->isaBase           = 0;
	gdev->pciBase           = DPU_BT_BASEADDR;

	gdev->frameAdrs         = DPU_BT_BASEADDR;
	gdev->memSize           = gdev->winSizeY * gdev->winSizeX * gdev->gdfBytesPP;

	gdev->vprBase           = DPU_BT_BASEADDR;
	gdev->cprBase           = DPU_BT_BASEADDR;

	return (void*)gdev;
}

int zynq_lcd_init(void)
{
	int wave_size = DPU_LAYER_WAVE_HEIGHT * DPU_LAYER_WAVE_WIDTH * 4;
//menu
	memset((char*)DPU_LE_BASEADDR, 0xCC, DPU_SCRN_WIDTH * DPU_SCRN_HEIGHT * 2);

//eye
	memset((char*)DPU_LE_BASEADDR, 0xCC, wave_size);

//wave
	memset((char*)DPU_LW_BASEADDR, 0xCC, wave_size);
	memset((char*)DPU_DW_BASEADDR, 0xCC, wave_size);

//back
	memset((char*)DPU_BT_BASEADDR, 0x0, DPU_SCRN_HEIGHT * DPU_SCRN_WIDTH * 2);
	memset((char*)DPU_LB_BASEADDR, 0x0, DPU_SCRN_HEIGHT * DPU_SCRN_WIDTH * 2);

	//lcd_reset();
	printf("DPU:   %x\n", drvDPUVersion());
	return 0;
}
