/*
 * hdmi_def_cfg.c
 *
 *  Created on: 2016年3月10日
 *      Author: sn02388 suoshichang
 */

#include "hdmi_def_cfg.h"

//是配置固定参数
//PLL时钟输入为200M的前提下
unsigned long HDMI_CLK[][6]=
{
	{ 0x008001c8, 0x00800042, 0x008004d4, 0x00fa0104, 0x7fe97c01, 0x08000800 },  //0      65M    & 325M
	{ 0x008001c8, 0x00800042, 0x008004d4, 0x00fa20c4, 0x7fe97c01, 0x80000800 },  //1      74.25M & 371.25M
	{ 0x00000145, 0x00000041, 0x0000069a, 0x00fa20c4, 0x7fe97c01, 0x80000800 },  //2      148.5M & 742.5M
	{ 0x00000145, 0x00000041, 0x0080034e, 0x015e2083, 0x7fe97c01, 0x01009000 },  //3      108M   & 540M
	{ 0x0000028a, 0x00000082, 0x00000082, 0x03e81041, 0x2fe92c01, 0x99001900 },  //4      40M    & 200M
	{ 0x00800597, 0x00800105, 0x00800209, 0x023f2042, 0x7fe97c01, 0x11009900 },  //5      25.175 & 125.875
	{ 0x00000514, 0x00000104, 0x0080034e, 0x015e2083, 0x7fe97c01, 0x01009000 },  //6      27M    & 135M
	{ 0x008001c8, 0x00800042, 0x00000410, 0x012c2083, 0x7fe97c01, 0x01009000 },  //7      85.5M  & 427.5M
	{ 0x00800514, 0x00800104, 0x008007e0, 0x00fa0145, 0x7fe97c01, 0x10000800 },  //8      31.5M  & 157.5M
};

/*******************************************************************************
 函 数 名:	devhdmiRegSet
 描    述:  DPU寄存器设置
 输入参数:

    unsigned long            *pu32RegAddr,        // 寄存器地址
    unsigned long             u32RegData          // 寄存器数据

 输出参数:  无
 返 回 值:
 说    明:
*******************************************************************************/
void devhdmiRegSet(unsigned long *pu32RegAddr, // 寄存器地址
                   unsigned long  u32RegData) // 寄存器数据
{
	*pu32RegAddr = u32RegData;
}
/*******************************************************************************
 函 数 名:devHDMI_RESET
 描    述    :hdmi复位，每次使能之后，都必须调用该函数
 输入参数:
 输出参数:
 返 回 值:
 说    明:
*******************************************************************************/

void devHDMI_RESET(void)
{
	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_RST_CFG)), HDMI_RST_SET(1));  //置1复位
	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_RST_CFG)), HDMI_RST_SET(0));  //置1复位
}

/*******************************************************************************
 函 数 名:  devHDMI_SET_NUM
 描    述:   直接设置hdmi接口参数
 输入参数: 12个：使能控制、显示区域首地址、10个配置参数
 输出参数: 无
 返 回 值:
 说    明:
*******************************************************************************/
void devHDMI_SET_NUM(unsigned long hdmi_en, unsigned long hdmidisaddr,
                     unsigned long h_count, unsigned long hsync_num,
                     unsigned long h_en_min, unsigned long h_en_max,
                     unsigned long v_count, unsigned long vsync_num,
                     unsigned long v_en_min, unsigned long v_en_max,
                     unsigned long pic_h_offset, unsigned long pic_v_offset,
                     unsigned long hdmi_pic_height, unsigned long hdmi_pic_width,
                     unsigned char hdmi_pic_data_type, unsigned char sync_polarity)
{
	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_PIC_BASEADDR_CFG)),
	              HDMI_BASE_ADDR_SET(hdmidisaddr));

	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_H_NUM_CFG)),
	              HDMI_H_NUM_SET(h_count, hsync_num, pic_h_offset));

	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_H_EN_NUM_CFG)),
	              HDMI_H_EN_NUM_SET(h_en_min, h_en_max));

	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_V_NUM_CFG)),
	              HDMI_V_NUM_SET(v_count, vsync_num, pic_v_offset));

	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_V_EN_NUM_CFG)),
	              HDMI_V_EN_NUM_SET(h_en_min, h_en_max, hdmi_en));

	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_PIC_CFG)),
	              HDMI_PIC_SET(hdmi_pic_height, hdmi_pic_width, hdmi_pic_data_type, sync_polarity));

	//devHDMI_RESET(); //复位操作
}

/*******************************************************************************
 函 数 名:  devHDMI_SET_Dis_Baseaddr
 描    述:   配置hdmi的显示基地址
 输入参数: unsigned long baseaddr:显示基地址
 输出参数: 无
 返 回 值:
 说    明:
*******************************************************************************/
void devHDMI_SET_Dis_Baseaddr(unsigned long baseaddr)
{
	devhdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_PIC_BASEADDR_CFG)), HDMI_BASE_ADDR_SET(baseaddr));
}

/*******************************************************************************
 函 数 名:	devhdmiRegSet
 描    述:   hdmi寄存器设置
 输入参数:

    unsigned long            *pu32RegAddr,        // 寄存器地址
    unsigned long             u32RegData          // 寄存器数据

 输出参数:  无
 返 回 值:
 说    明:
*******************************************************************************/
void devHdmiRegSet(unsigned long *pu32RegAddr, // 寄存器地址
                   unsigned long  u32RegData)   // 寄存器数据
{
	*pu32RegAddr = u32RegData;
}
/*******************************************************************************
 函 数 名: devHDMI_SET_CFG
 描    述: hdmi 寄存器配置
 输入参数:  ST_HDMI_D  *hdmi_cfg:hdmi配置参数
 输出参数:  无
 返 回 值:
 说    明:
*******************************************************************************/
void devHDMI_SET_CFG(ST_HDMI_D *hdmi_cfg)
{
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_PIC_BASEADDR_CFG))
	              HDMI_BASE_ADDR_SET(hdmi_cfg->hdmi_pic_baseaddr));

	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_H_NUM_CFG)),
	              HDMI_H_NUM_SET(hdmi_cfg->hdmi_h_num, hdmi_cfg->hdmi_h_sync_num, hdmi_cfg->hdmi_h_pic_offset));

	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_H_EN_NUM_CFG)),
	              HDMI_H_EN_NUM_SET(hdmi_cfg->hdmi_h_en_min, hdmi_cfg->hdmi_h_en_max));

	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_V_NUM_CFG)),
	              HDMI_V_NUM_SET(hdmi_cfg->hdmi_v_num, hdmi_cfg->hdmi_v_sync_num, hdmi_cfg->hdmi_v_pic_offset));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_PIC_CFG)),
	              HDMI_PIC_SET(hdmi_cfg->hdmi_pic_height, hdmi_cfg->hdmi_pic_width, hdmi_cfg->hdmi_pic_data_type, hdmi_cfg->hdmi_sync_polarity));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_V_EN_NUM_CFG)),
	              HDMI_V_EN_NUM_SET(hdmi_cfg->hdmi_v_en_min, hdmi_cfg->hdmi_v_en_max, hdmi_cfg->hdmi_en));

	//devHDMI_RESET(); //复位操作
}

/*******************************************************************************
 函 数 名:  devHDMI_CLK_CFG
 描    述:   hdmi 时钟配置
 输入参数:  unsigned char hdmi_definition:预先定义的分辨率
 输出参数:  无
 返 回 值:
 说    明:
*******************************************************************************/
void devHDMI_CLK_CFG(unsigned char hdmi_definition)
{
	unsigned long *cfg_p;

	switch(hdmi_definition){
	case HDMI_1024_768_60:
		cfg_p = HDMI_CLK[0];
		break;    //  65M
	case HDMI_1280_960_60:
		cfg_p = HDMI_CLK[3];
		break;    //  108M
	case HDMI_1152_864_75:
		cfg_p = HDMI_CLK[3];
		break;    //  108M
	case HDMI_800_600_60:
		cfg_p = HDMI_CLK[4];
		break;    //  40M
	case HDMI_1280_1024_60:
		cfg_p = HDMI_CLK[3];
		break;    //  108M
	case HDMI_640_480_60:
		cfg_p = HDMI_CLK[5];
		break;	  //  25.175M
	case HDMI_640_480_75:
		cfg_p = HDMI_CLK[8];
		break;    //  31.5M
	case HDMI_720_576_50:
		cfg_p = HDMI_CLK[6];
		break;    //  27M
	case HDMI_1280_720_60:
		cfg_p = HDMI_CLK[1];
		break;    //  74.25M
	case HDMI_1920_1080_60:
		cfg_p = HDMI_CLK[2]
			break;    //  148.5M
	case HDMI_720_480_60:
		cfg_p = HDMI_CLK[6];
		break;	  //  27M
	case HDMI_1360_768_60:
		cfg_p = HDMI_CLK[7];
		break;	  //  85.5M
	default:
		cfg_p = HDMI_CLK[0];
		break;    //默认情况下采用1024*768@60Hz分辨率
	}

	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFG0)), *(cfg_p++));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFG1)), *(cfg_p++));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFG2)), *(cfg_p++));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFG3)), *(cfg_p++));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFG4)), *(cfg_p++));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFG5)), *(cfg_p));
	devHdmiRegSet((unsigned long*)(HDMI_SET_CFG(HDMI_CLK_CFGEN)), HDMI_CLK_CFGEN_SET(1));
}

/*******************************************************************************
 函 数 名:	devDPUHDMI_SET_DEF
 描    述:   hdmi 参数配置
 输入参数:   unsigned char hdmi_definition :分辨率宏定义（选择配置哪种分辨率）
		 unsigned long baseaddr:显示首地址
		 unsigned long hdmi_en:hdmi使能控制
 输出参数:  无
 返 回 值:
 说    明:  顶层调用接口
*******************************************************************************/
int devDPUHDMI_SET_DEF(unsigned char hdmi_definition, unsigned long baseaddr,
                       unsigned long width, unsigned long height,
                       unsigned char datatype, unsigned long hdmi_en)
{
	ST_HDMI_D hdmi_data;  //临时变量
	unsigned char status;  //状态控制

	hdmi_data.hdmi_pic_baseaddr = baseaddr;
	hdmi_data.hdmi_en = hdmi_en;
	hdmi_data.hdmi_pic_width = width;
	hdmi_data.hdmi_pic_height = height;
	hdmi_data.hdmi_pic_data_type = datatype;

	switch (hdmi_definition) {
	case HDMI_1024_768_60:         //     (4:3)
		hdmi_data.hdmi_h_num = 1344;
		hdmi_data.hdmi_h_sync_num = 136;
		hdmi_data.hdmi_h_en_min = 296;
		hdmi_data.hdmi_h_en_max = 1320;

		hdmi_data.hdmi_v_num = 806;
		hdmi_data.hdmi_v_sync_num = 6;
		hdmi_data.hdmi_v_en_min = 35;
		hdmi_data.hdmi_v_en_max = 803;

		hdmi_data.hdmi_h_pic_offset = 0;
		hdmi_data.hdmi_v_pic_offset = 84;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	case HDMI_1280_960_60:    //      (4:3)
		hdmi_data.hdmi_h_num = 1800;
		hdmi_data.hdmi_h_sync_num= 112;
		hdmi_data.hdmi_h_en_min = 424;
		hdmi_data.hdmi_h_en_max = 1704;

		hdmi_data.hdmi_v_num = 1000;
		hdmi_data.hdmi_v_sync_num = 3;
		hdmi_data.hdmi_v_en_min = 39;
		hdmi_data.hdmi_v_en_max = 999;

		hdmi_data.hdmi_h_pic_offset = 128;
		hdmi_data.hdmi_v_pic_offset = 180;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	case HDMI_1152_864_75:     //    (4:3)
		hdmi_data.hdmi_h_num = 1600;
		hdmi_data.hdmi_h_sync_num = 128;
		hdmi_data.hdmi_h_en_min = 384;
		hdmi_data.hdmi_h_en_max = 1536;

		hdmi_data.hdmi_v_num = 900;
		hdmi_data.hdmi_v_sync_num = 3;
		hdmi_data.hdmi_v_en_min = 35;
		hdmi_data.hdmi_v_en_max = 899;

		hdmi_data.hdmi_h_pic_offset = 64;
		hdmi_data.hdmi_v_pic_offset = 132;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
	break;
	case  HDMI_800_600_60:       // (4:3)
		hdmi_data.hdmi_h_num = 1056;
		hdmi_data.hdmi_h_sync_num = 128;
		hdmi_data.hdmi_h_en_min = 216;
		hdmi_data.hdmi_h_en_max = 1016;

		hdmi_data.hdmi_v_num = 628;
		hdmi_data.hdmi_v_sync_num = 4;
		hdmi_data.hdmi_v_en_min = 27;
		hdmi_data.hdmi_v_en_max = 627;

		hdmi_data.hdmi_h_pic_offset = 0;
		hdmi_data.hdmi_v_pic_offset = 0;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	case HDMI_1280_1024_60:      // (5:4)
		hdmi_data.hdmi_h_num = 1688;
		hdmi_data.hdmi_h_sync_num = 112;
		hdmi_data.hdmi_h_en_min = 360;
		hdmi_data.hdmi_h_en_max = 1640;

		hdmi_data.hdmi_v_num = 1066;
		hdmi_data.hdmi_v_sync_num = 3;
		hdmi_data.hdmi_v_en_min = 41;
		hdmi_data.hdmi_v_en_max = 1065;

		hdmi_data.hdmi_h_pic_offset = 128;
		hdmi_data.hdmi_v_pic_offset = 212;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	case HDMI_640_480_60:
		hdmi_data.hdmi_h_num = 800;
		hdmi_data.hdmi_h_sync_num = 96;
		hdmi_data.hdmi_h_en_min = 136;
		hdmi_data.hdmi_h_en_max = 776;

		hdmi_data.hdmi_v_num = 525;
		hdmi_data.hdmi_v_sync_num = 2;
		hdmi_data.hdmi_v_en_min = 27;
		hdmi_data.hdmi_v_en_max = 507;

		hdmi_data.hdmi_h_pic_offset = 0;
		hdmi_data.hdmi_v_pic_offset = 0;
		hdmi_data.hdmi_sync_polarity = 0;

		status = 0;
		break;
	case HDMI_640_480_75:
	{
		hdmi_data.hdmi_h_num = 840;
		hdmi_data.hdmi_h_sync_num = 64;
		hdmi_data.hdmi_h_en_min = 184;
		hdmi_data.hdmi_h_en_max = 824;

		hdmi_data.hdmi_v_num = 500;
		hdmi_data.hdmi_v_sync_num = 3;
		hdmi_data.hdmi_v_en_min = 19;
		hdmi_data.hdmi_v_en_max = 499;

		hdmi_data.hdmi_h_pic_offset = 0;
		hdmi_data.hdmi_v_pic_offset = 0;
		hdmi_data.hdmi_sync_polarity = 0;

		status = 0;
		break;
	case HDMI_720_576_50:
		hdmi_data.hdmi_h_num = 864;
		hdmi_data.hdmi_h_sync_num = 64;
		hdmi_data.hdmi_h_en_min = 132;
		hdmi_data.hdmi_h_en_max = 852;

		hdmi_data.hdmi_v_num = 625;
		hdmi_data.hdmi_v_sync_num = 5;
		hdmi_data.hdmi_v_en_min = 44;
		hdmi_data.hdmi_v_en_max = 620;

		hdmi_data.hdmi_h_pic_offset = 0;
		hdmi_data.hdmi_v_pic_offset = 0;
		hdmi_data.hdmi_sync_polarity = 0;

		status = 0;
		break;
	case HDMI_1280_720_60:    //5          (16:9)
		hdmi_data.hdmi_h_num = 1650;
		hdmi_data.hdmi_h_sync_num = 40;
		hdmi_data.hdmi_h_en_min = 260;
		hdmi_data.hdmi_h_en_max = 1540;

		hdmi_data.hdmi_v_num = 750;
		hdmi_data.hdmi_v_sync_num = 5;
		hdmi_data.hdmi_v_en_min = 25;
		hdmi_data.hdmi_v_en_max = 745;

		hdmi_data.hdmi_h_pic_offset = 128;
		hdmi_data.hdmi_v_pic_offset = 60;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	case HDMI_1920_1080_60:
		hdmi_data.hdmi_h_num = 2200;
		hdmi_data.hdmi_h_sync_num = 44;
		hdmi_data.hdmi_h_en_min = 192;
		hdmi_data.hdmi_h_en_max = 2112;

		hdmi_data.hdmi_v_num = 1125;
		hdmi_data.hdmi_v_sync_num = 5;
		hdmi_data.hdmi_v_en_min = 41;
		hdmi_data.hdmi_v_en_max = 1121;

		hdmi_data.hdmi_h_pic_offset = 448;
		hdmi_data.hdmi_v_pic_offset = 240;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	case HDMI_720_480_60:
		hdmi_data.hdmi_h_num = 858;
		hdmi_data.hdmi_h_sync_num = 62;
		hdmi_data.hdmi_h_en_min = 122;
		hdmi_data.hdmi_h_en_max = 842;

		hdmi_data.hdmi_v_num = 525;
		hdmi_data.hdmi_v_sync_num = 6;
		hdmi_data.hdmi_v_en_min = 42;
		hdmi_data.hdmi_v_en_max = 522;

		hdmi_data.hdmi_h_pic_offset = 0;
		hdmi_data.hdmi_v_pic_offset = 0;
		hdmi_data.hdmi_sync_polarity = 0;

		status = 0;
		break;
	case HDMI_1360_768_60:
		hdmi_data.hdmi_h_num = 1792;
		hdmi_data.hdmi_h_sync_num = 112;
		hdmi_data.hdmi_h_en_min = 368;
		hdmi_data.hdmi_h_en_max = 1728;

		hdmi_data.hdmi_v_num = 795;
		hdmi_data.hdmi_v_sync_num = 6;
		hdmi_data.hdmi_v_en_min = 24;
		hdmi_data.hdmi_v_en_max = 792;

		hdmi_data.hdmi_h_pic_offset = 168;
		hdmi_data.hdmi_v_pic_offset = 84;
		hdmi_data.hdmi_sync_polarity = 3;

		status = 0;
		break;
	default:
		status = 1;
		break;
	}

	if (!status) {
		devHDMI_SET_CFG(&hdmi_data);
		return 0;
	} else {
		return 1;
	}
}

/*******************************************************************************
 函 数 名:	EDID_Data_vld
 描    述:   判断读取的EDID序列号是否有效，现认为头正确就是正确的EDID序列号
 输入参数:  unsigned char dattemp[]:EDID号数组首地址
 输出参数:  无
 返 回 值:
 说    明:  顶层调用接口
//判断范围EDID[0] - [7]
*******************************************************************************/
int EDID_Data_vld(unsigned char dattemp[])
{
	if ((dattemp[0] == 0x00) &&
	    (dattemp[1] == 0xff) &&
	    (dattemp[2] == 0xff) &&
	    (dattemp[3] == 0xff) &&
	    (dattemp[4] == 0xff) &&
	    (dattemp[5] == 0xff) &&
	    (dattemp[6] == 0xff) &&
	    (dattemp[7] == 0x00))
		return 1;
	else
		return 0;
}

/*******************************************************************************
 函 数 名:  Judge_ManufacturerID
 描    述:   读取厂商标识
 输入参数:  dattemp[] :EDID序列号数组

 输出参数:  Manufac_Inf :屏幕信息
 返 回 值:
 说    明:  顶层调用接口
//判断范围EDID[8] - [17]
*******************************************************************************/
void Judge_ManufacturerID(unsigned char dattemp[], Manufac_Inf *manufac_inf)
{
	unsigned char temp[3];

	temp[0] =  (dattemp[8] & 0x7c) >> 2;
	temp[1] = ((dattemp[8] & 0x03) << 3) + ((dattemp[9] & 0xe0) >> 5);
	temp[2] =  (dattemp[9] & 0x1f);

	//厂商标识
	manufac_inf->Manufac_ID[0] = temp[0] + 0x40;  //转换成ASCII
	manufac_inf->Manufac_ID[1] = temp[1] + 0x40;
	manufac_inf->Manufac_ID[2] = temp[2] + 0x40;

	//产品序列号
	manufac_inf->Product_ID = (dattemp[11] << 8) | dattemp[10];

	//产品流水号
	manufac_inf->Product_Serial = (dattemp[15] << 24) |
	                              (dattemp[14] << 16) |
	                              (dattemp[13] << 8) |
	                               dattemp[12]; //最低位

	//产品生产当年的第N个周
	if ((dattemp[16] >= 0x01) &&
	    (dattemp[16] <= 0x36))
		manufac_inf->Week = dattemp[16];

	if(dattemp[17] >= 0x10)
		manufac_inf->Year = dattemp[17] + 1990;
}


/*******************************************************************************
 函 数 名:  Judge_ManufacturerID
 描    述:   EDID版本号
 输入参数:  dattemp[] :EDID序列号数组

 输出参数:  EDID版本号
 返 回 值:
 说    明:  顶层调用接口
//判断范围EDID[18] - [19]
*******************************************************************************/
void Judge_EDID_Version(unsigned char dattemp[], EDID_Version *edid_version)
{
	edid_version->version = dattemp[18];
	edid_version->revision = dattemp[19];
}

/*******************************************************************************
 函 数 名:  Judge_Established_Timing
 描    述:   判断支持的內建时序种类
 输入参数:  unsigned char dattemp[]:EDID序列号数组
 输出参数:  unsigned char*Established_Timing:采用2个byte长度的数组存内建标准
 返 回 值:
 说    明: 具体bit,1代表支持相应的分辨率，0代表不支持
      使用2bytes空间接口

//判断范围EDID[35] - [36]
*******************************************************************************/

void Judge_Established_Timing(unsigned char dattemp[], unsigned char *Established_Timing)
{
	Established_Timing[1] = dattemp[36];
	Established_Timing[0] = dattemp[35];  //返回16位数
}

/*******************************************************************************
 函 数 名:  Judge_Standard_Timing
 描    述:   判断支持的标准时序
 输入参数:  unsigned char dattemp[]: EDID数组头指针

 输出参数:  VidRes_D Res_Standrad[] :标准时序数组头指针
 返 回 值:
 说    明:
 //判断范围EDID[38] - [53]
*******************************************************************************/
void Judge_Standard_Timing(unsigned char dattemp[], Standard_Tim *Res_Standrad)
{
	unsigned char i, num;

	num = 0;
	for( i = 0; i < 7; i++) {  //8个标准內建时序
		if (dattemp[38 + i * 2] != 0x01 &&
		    dattemp[38 + i * 2 + 1] != 0x01) {  //当数据有效时进行计算
			Res_Standrad->Std_Tim[num].hnum = dattemp[38 + i * 2] * 8 + 248;  //计算 行分辨率
			switch (dattemp[38 + i * 2 + 1] & 0xc0) {
			case 0x00:
				Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 16 * 10;//计算 列分辨率
				Res_Standrad->Std_Tim[num].radio = RADIO_16_10;
				break;
			case 0x40:
				Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 4 * 3;//计算 列分辨率
				Res_Standrad->Std_Tim[num].radio = RADIO_4_3;
				break;
			case 0x80:
				Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 5 * 4;//计算 列分辨率
				Res_Standrad->Std_Tim[num].radio = RADIO_5_4;
				break;
			case 0xc0:
				Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 16 * 9;//计算 列分辨率
				Res_Standrad->Std_Tim[num].radio = RADIO_16_9;
				break;
			}
			Res_Standrad->Std_Tim[num].freq = (dattemp[38 + i * 2 + 1] & 0x3f) + 60;   //场刷新率计算
			num = num + 1;
		}
	}

	Res_Standrad->Stand_Tim_Num = num;  //记录标准时序个数
}

/*******************************************************************************
 函 数 名:  Judge_Preferred_Timing
 描    述:   判断最佳分辨率详细参数
 输入参数:  unsigned char dattemp[]: EDID序列号

 输出参数:  VideoRes_D *Res_PreferredTime:最佳分辨率的详细信息（可用来配置）
 返 回 值:
 说    明:Res_PreferredTim

 //判断范围EDID[54] - [71]
*******************************************************************************/
void Judge_Preferred_Timing(unsigned char dattemp[],Detaild_Time *Res_PreferredTime)
{
	unsigned short hnum;
	unsigned short vnum;
	float radiotmp;

	//频率解析
	Res_PreferredTime->Pixel_clk = (dattemp[55] << 8) + dattemp[54];  //频率解析

	//行数
	hnum = ((dattemp[58] & 0xf0) << 4) + dattemp[56];
	Res_PreferredTime->Haddressable = hnum;

	//行无效像素
	Res_PreferredTime->Hblacking = ((dattemp[58] & 0x0f) << 8) + dattemp[57];

	//
	Res_PreferredTime->HFrontPorch = ((dattemp[65] & 0xc0) << 2) + dattemp[62];
	Res_PreferredTime->HSyncPorch =  ((dattemp[65] & 0x30) << 4) + dattemp[63];

	//列数
	vnum = ((dattemp[61] & 0xf0) << 4) + dattemp[59];
	Res_PreferredTime->Vaddressable = ((dattemp[61] & 0xf0) << 4) + dattemp[59];

	//无效列数
	Res_PreferredTime->Vblacking = ((dattemp[61] & 0x0f) << 8) + dattemp[60];

	//
	Res_PreferredTime->VFrontPorch = ((dattemp[65] & 0x0c) << 2) + ((dattemp[64] & 0xf0) >> 4);
	Res_PreferredTime->VSyncPorch =  ((dattemp[65] & 0x03) << 4) + (dattemp[64] & 0x0f);

	//判断长宽比
	radiotmp = (float)hnum / (float)vnum;
	if ((radiotmp > 1.55) &&
	    (radiotmp < 1.65))          //16:10
		Res_PreferredTime->radio = RADIO_16_10;
	else if ((radiotmp > 1.30) &&
	         (radiotmp < 1.40))    //4:3
		Res_PreferredTime->radio = RADIO_4_3;
	else if ((radiotmp > 1.20) &&
	         (radiotmp < 1.30))    //5:4
		Res_PreferredTime->radio = RADIO_5_4;
	else if ((radiotmp > 1.70) &&
	         (radiotmp < 1.80))    //16:9
		Res_PreferredTime->radio = RADIO_16_9;
	else
		Res_PreferredTime->radio = 0;
}

/*******************************************************************************
 函 数 名:Juding_DisRange_limit
 描    述:   判断该屏幕参数的可设置范围
 输入参数:   unsigned char dattemp[]: EDID 号码输入

 输出参数:	  unsigned char *Established_Tim_III, //内建时序需要5个字节接收,可能存在
		  Standard_Tim *Res_Standrad,     :支持的标准时序，可能存在
		  Tim_Lit *tim_lit:                参数范围
		  unsigned char *ProName:     （ascll 输出，需要用长度为13的数组接收）
 返 回 值 :
 说    明: 得出特定屏幕的显示分辨率频率范围
 //判断范围EDID[72] - [125]
*******************************************************************************/
void Juding_DisRange_limit(unsigned char dattemp[],      //EDID数组
                           Tim_Lit *tim_lit,             //设置范围 ,一定有
                           Standard_Tim *Res_Standrad,   //标准时序，可能存在
                           unsigned char *Established_Tim_III,  //需要5个字节接收
                           unsigned char  ProName)              //product name,一定有
{
	unsigned char i, j, num;

	for (i = 0; i < 3; i++) {    //判断第2、3、4 个18字节详细描述区域
		if ((dattemp[72 + i * 18 + 0] == 0) &&
		    (dattemp[72 + i * 18 + 1] == 0) &&
		    (dattemp[72 + i * 18 + 2] == 0)) {  //表明为display descriptor描述段
			switch(dattemp[72 + i * 18 + 3]) {   //判断18字节段的具体意义
			case 0xff://display product serial number
				//for(j=0;j<13;j++) DisProSerNum[j] = dattemp[72+i*18+5+j];//存储serial number  ,不关注屏蔽掉
				break;
			case 0xfe://alphanumeric data string(asill)
				//for(j=0;j<13;j++) alphanum[j] = dattemp[72+i*18+5+j];    //存储alphanumeric data ,不关注屏蔽掉
				break;
			case 0xfd://display range limits
				//垂直
				switch(dattemp[72 + i * 18 + 4] & 0x03) { //Vertical rate范围是否需要加数据偏移
				case 0x00: //数据不加偏移
					tim_lit->Min_v = dattemp[72 + i * 18 + 5];
					tim_lit->Max_v = dattemp[72 + i * 18 + 6];
					break;
				case 0x02: //Max加偏移，Min不加偏移
					tim_lit->Min_v = dattemp[72 + i * 18 + 5];
					tim_lit->Max_v = dattemp[72 + i * 18 + 6] +255;
					break;
				case 0x03://都加偏移
					tim_lit->Min_v = dattemp[72 + i * 18 + 5] + 255;
					tim_lit->Max_v = dattemp[72 + i * 18 + 6] + 255;
					break;
				default:
					break;
				}
				//水平速率
				switch(dattemp[72+i*18+4] & 0x0c) {
				case 0x00://数据不加偏移
					tim_lit->Min_h = dattemp[72 + i * 18 + 7];
					tim_lit->Max_h = dattemp[72 + i * 18 + 8];
					break;
				case 0x08://Max加偏移，Min不加偏移
					tim_lit->Min_h = dattemp[72 + i * 18 + 7];
					tim_lit->Max_h = dattemp[72 + i * 18 + 8] + 255;
					break;
				case 0x0c://都加偏移
					tim_lit->Min_h = dattemp[72 + i * 18 + 7] + 255;
					tim_lit->Max_h = dattemp[72 + i * 18 + 8] + 255;
					break;
				default:
					break;
				}

				tim_lit->Max_pixel_clk = dattemp[72 + i * 18 + 9] * 10;  //最大pixel_clk
				break;
			case 0xfc: //displayproduct name
				for (j = 0; j < 13; j++)
					ProName[j] = dattemp[72 + i * 18 + 5 + j];    //存储alphanumeric data
				break;
			case 0xfb: //color point data
				break;
			case 0xfa: //standard timing identifications
				for (j = 0; j < 6; j++) {  //最多六项
					if ((dattemp[72 + i * 18 + 5 + j * 2] != 0x01) &&
					    (dattemp[72 + i * 18 + 5 + j * 2 + 1] != 0x01)) {  //当数据有效时进行计算
						Res_Standrad->Std_Tim[num].hnum = dattemp[72 + i * 18 + 5 + j * 2] * 8 + 248;  //计算 行分辨率
						switch(dattemp[72 + i * 18 + 5 + j * 2 + 1] & 0xc0) {
						case 0x00:
							Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 16 * 10;//计算 列分辨率
							Res_Standrad->Std_Tim[num].radio = RADIO_16_10;
							break;
						case 0x40:
							Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 4 * 3;//计算 列分辨率
							Res_Standrad->Std_Tim[num].radio = RADIO_4_3;
							break;
						case 0x80:
							Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 5 * 4;//计算 列分辨率
							Res_Standrad->Std_Tim[num].radio = RADIO_5_4;
							break;
						case 0xc0:
							Res_Standrad->Std_Tim[num].vnum = Res_Standrad->Std_Tim[num].hnum / 16 * 9;//计算 列分辨率
							Res_Standrad->Std_Tim[num].radio = RADIO_16_9;
							break;
						}
						Res_Standrad->Std_Tim[num].freq = (dattemp[72 + i * 18 + 5 + j * 2 + 1] & 0x3f) + 60;   //场刷新率计算
						num = num + 1;
					}
				}
				Res_Standrad->Stand_Tim_Num = num;  //记录标准时序个数
				break;
			case 0xf9: //display color management
				break;
			case 0xf8: //CVT 3 byte timing codes
				break;
			case 0xf7: //established timing III
				for (j = 0; j < 6; j++)
					Established_Tim_III[j] = dattemp[72 + i * 18 + 6 + j];    //存储established timing III支持的分辨率
				break;
			case 0x10: //dummy descriptor
				break;
			default:
				break;
			}//switch(dattemp[72+i*18+3])
		}//if()
	}//for(i=0;i<3;i++)
}


/*******************************************************************************
 函 数 名:  extension_vld
 描    述:   判断是否存在扩展字符
 输入参数:  unsigned char dattemp[]: EDID 数组输入

 输出参数:  1：表明存在扩展字节，  0表明不存在扩展字节
 返 回 值 :
 说    明:
 //判断范围EDID[126]
*******************************************************************************/
int extension_vld(unsigned char dattemp[])
{
	return (dattemp[126] ? 1 : 0);
}


/*******************************************************************************
 函 数 名:Judge_CEA_Extension_Version
 描    述:   判断扩展字符
 输入参数:  unsigned char dattemp[]: EDID序列号

 输出参数:  EDID_Version *extension_ver
 返 回 值:
 说    明:

 //判断范围EDID[128 + 0] - [128 + 1]
*******************************************************************************/
void Judge_CEA_Extension_Version(unsigned char dattemp[], EDID_Version *extension_ver)
{
	extension_ver->version =  dattemp[128 + 0];
	extension_ver->revision = dattemp[128 + 1];
}

/*******************************************************************************
 函 数 名:  extension_vld
 描    述:   判断扩展字符中连续的18字节区域
 输入参数:  unsigned char dattemp[]: EDID 数组输入

 输出参数:  Detaild_Tim* Res_extendTim :详细参数描述
 返 回 值 :
 说    明:
 //判断范围EDID[128+EDID[130]] - EDID[128+EDID[130]+18*n]
*******************************************************************************/
void Exten_Detail_Tim(unsigned char dattemp[], Detaild_Tim *Res_extendTim)
{
	unsigned char i,num;
	unsigned short hnum;
	unsigned short vnum;
	float radiotmp;

	//计算长度
	i = 0xfe;
	while (dattemp[i] == 0)
		i--;

	num = (i- (128 + dattemp[130] + 1)) / 18;      //计算出扩展块的个数

	Res_extendTim->Detaild_Tim_Num = num;  //记录有效的18bytes块的总个数
	for (i = 0; i < num; i++) {
		//频率解析
		Res_extendTim->Detaild_Tim[i].Pixel_clk = (dattemp[128 + dattemp[130] + i * 18 + 1] << 8) +
		                                           dattemp[128 + dattemp[130] + i * 18];  //频率解析

		//行数
		hnum = ((dattemp[128 + dattemp[130] + i * 18 + 4] & 0xf0) << 4) +
		         dattemp[128 + dattemp[130] + i * 18 + 2];
		Res_extendTim->Detaild_Tim[i].Haddressable = hnum;

		//行无效像素个数
		Res_extendTim->Detaild_Tim[i].Hblacking = ((dattemp[128 + dattemp[130] + i * 18 + 4] & 0x0f) << 8) +
		                                            dattemp[128 + dattemp[130] + i * 18 + 3];

		//列数
		vnum = ((dattemp[128 + dattemp[130] + i * 18 + 7] & 0xf0) << 4) +
		         dattemp[128 + dattemp[130] + i * 18 + 5];
		Res_extendTim->Detaild_Tim[i].Vaddressable = vnum;

		Res_extendTim->Detaild_Tim[i].Vblacking = ((dattemp[128 + dattemp[130] + i * 18 + 7] & 0x0f) << 8) +
		                                            dattemp[128 + dattemp[130] + i * 18 + 6];

		Res_extendTim->Detaild_Tim[i].HFrontPorch = ((dattemp[128 + dattemp[130] + i * 18 + 11] & 0xc0) << 2) +
		                                              dattemp[128 + dattemp[130] + i * 18 + 8];

		Res_extendTim->Detaild_Tim[i].HSyncPorch =((dattemp[128 + dattemp[130] + i * 18 + 11] & 0x30) << 4) +
		                                            dattemp[63];

		Res_extendTim->Detaild_Tim[i].VFrontPorch = ((dattemp[128 + dattemp[130] + i * 18 + 11] & 0x0c) << 2)+
		                                            ((dattemp[128 + dattemp[130] + i * 18 + 10] & 0xf0) >> 4);

		Res_extendTim->Detaild_Tim[i].VSyncPorch = ((dattemp[128 + dattemp[130] + i * 18 + 11] & 0x03) << 4) +
		                                            (dattemp[128 + dattemp[130] + i * 18 + 10] & 0x0f);

		//判断长宽比
		radiotmp = (float)hnum / (float)vnum;
		if ((radiotmp > 1.55) &&
		    (radiotmp<1.65))         //16:10
			Res_extendTim->Detaild_Tim[i].radio = RADIO_16_10;
		else if ((radiotmp > 1.30) &&
		         (radiotmp < 1.40))    //4:3
			Res_extendTim->Detaild_Tim[i].radio = RADIO_4_3;
		else if ((radiotmp > 1.20) && (radiotmp < 1.30))    //5:4
			Res_extendTim->Detaild_Tim[i].radio = RADIO_5_4;
		else if ((radiotmp > 1.70) &&
		         (radiotmp < 1.80))    //16:9
			Res_extendTim->Detaild_Tim[i].radio = RADIO_16_9;
		else
			Res_extendTim->Detaild_Tim[i].radio = 0;
	}

}


/*******************************************************************************
 函 数 名:  Search_Standard_Tim
 描    述:   Standard_Tim* Res_Standrad_Tim:标准时序头指针
	    unsigned short hnum:搜索项：行数
	    unsigned short vnum:搜索项：列数
 输入参数:  无
 输出参数:  返回1表明找到，否则没有找到
 返 回 值:
 说    明:
*******************************************************************************/
unsigned char Search_Standard_Tim(Standard_Tim *Res_Standrad_Tim,
                                  unsigned short hnum, unsigned short vnum)
{
	unsigned char i;

	for (i = 0; i < Res_Standrad_Tim->Stand_Tim_Num; i++) {
		if ((Res_Standrad_Tim->Std_Tim[i].hnum == hnum) &&
		    (Res_Standrad_Tim->Std_Tim[i].vnum == vnum))
			return 1;
	}
	return 0;
}

/*******************************************************************************
 函 数 名:  Search_Standard_Tim
 描    述:   Standard_Tim* Res_Standrad_Tim:标准时序头指针
	    unsigned short hnum:搜索项：行数
	    unsigned short vnum:搜索项：列数
 输入参数:  无
 输出参数:  无
 返 回 值: 返回1表明找到，否则没有找到
 说    明:
*******************************************************************************/
unsigned char Search_ExtendTim_Tim(unsigned char dattemp[], Detaild_Tim *Res_extendTim,
                                   unsigned short hnum, unsigned short vnum)
{
	unsigned char i;

	if (extension_vld(dattemp)) {  //如果存在扩展项
		for (i = 0; i < Res_extendTim->Detaild_Tim_Num; i++) {
			if ((Res_extendTim->Detaild_Tim[i].Haddressable == hnum) &&
			    (Res_extendTim->Detaild_Tim[i].Vaddressable == vnum))
				return 1;
		}
	}

	return 0;
}

/*******************************************************************************
 函 数 名:  Convert_dataildtim2param
 描    述  :  将EDID内部存储的详细参数转变为可配置FPGA的参数（10个参数必须转换）

 输入参数: Detaild_Time* Res_extendTime:详细时序描述

 输出参数: ST_HDMI_D   Hdmi_param:hdmi配置的参数输出
 返 回 值:
 说    明 :测试通过
*******************************************************************************/
void Convert_dataildtim2param(Detaild_Time *Res_extendTime, ST_HDMI_D *Hdmi_param)
{
	Hdmi_param->hdmi_h_num = Res_extendTime->Haddressable + Res_extendTime->Hblacking; //行数加控制
	Hdmi_param->hdmi_h_sync_num = Res_extendTime->HSyncPorch;
	Hdmi_param->hdmi_h_en_min = Res_extendTime->Hblacking - Res_extendTime->HFrontPorch;
	Hdmi_param->hdmi_h_en_max = Res_extendTime->Haddressable +
	                            (Res_extendTime->Hblacking - Res_extendTime->HFrontPorch);

	Hdmi_param->hdmi_v_num = Res_extendTime->Vaddressable + Res_extendTime->Vblacking;
	Hdmi_param->hdmi_v_sync_num = Res_extendTime->VSyncPorch;
	Hdmi_param->hdmi_v_en_min = Res_extendTime->Vblacking - Res_extendTime->VFrontPorch;
	Hdmi_param->hdmi_v_en_max = Res_extendTime->Vaddressable +
	                            (Res_extendTime->Vblacking - Res_extendTime->VFrontPorch);

	Hdmi_param->hdmi_h_pic_offset = Hdmi_param->hdmi_h_en_min + (Res_extendTime->Haddressable - 1024) / 2;
	Hdmi_param->hdmi_v_pic_offset = Hdmi_param->hdmi_v_en_min + (Res_extendTime->Vaddressable - 600) / 2;
}


/*******************************************************************************
 函 数 名:  SetHDMI_perfect_res
 描    述:   根据判断设置最佳分辨率（决策函数）
 输入参数:  unsigned char dattemp[]:EDID序列号
         unsigned long baseaddr           :图层基地址
 输出参数:  无
 返 回 值:
 说    明:
//1、  对于4:3和5:4比例的屏幕设置为1024*768分辨率,如果不支持,根据日后情况如果日后时钟可以灵活设置
 *   那么可以根据EDID信息中的详细参数列表灵活设置参数，否则表明不支持。
 2、   对于16:9和16:10比例的屏幕设置为1280*720分辨率，如果不支持,根据日后情况如果日后时钟可以灵活设置
 *   那么可以根据EDID信息中的详细参数列表灵活设置参数，否则表明不支持。
*******************************************************************************/
int SetHDMI_perfect_res(unsigned char dattemp[], unsigned long baseaddr,
                        unsigned long width, unsigned long height,
                        unsigned char datatype, unsigned char en_ctl)
{
	unsigned char sucess_flg;
	unsigned char ProName[13];            //产品名称
	//16个內建时序支持存储空间
	unsigned char established_tim_I_II[2];  //通过宏定义判断
	unsigned char established_tim_III[6];
	//8个标准时序支持存储空间
	Standard_Tim Res_Standrad_Tim_temp_base1;     //存储支持的分辨率大小和数量
	Standard_Tim Res_Standrad_Tim_temp_base2;     //存储支持的分辨率大小和数量,可能存在

	Tim_Lit Tim_limit;

	//1个最佳时序存储空间
	Detaild_Time Res_PreferredTim_temp;           //存储最佳分辨率     ,一定存在
	Detaild_Tim Res_extendTim;                    //在扩展时序中存储的详细分辨率参数

	Judge_Established_Timing(dattemp, established_tim_I_II);           //內建时序的提取
	Judge_Standard_Timing(dattemp, &Res_Standrad_Tim_temp_base1);   //标准时序提取
	Judge_Preferred_Timing(dattemp, &Res_PreferredTim_temp);         //最佳分辨率提取
	Juding_DisRange_limit(dattemp, &Tim_limit,&Res_Standrad_Tim_temp_base2,
	                      established_tim_III, ProName);  //提取限制范围，可能提取标准时序

	//如果存在扩展序列，则提取扩展序列的详细参数
	if (extension_vld(dattemp))
		Exten_Detail_Tim(dattemp,&Res_extendTim);    //解析扩展序列

	//决策判断16:9 & 16:10 比例的屏幕分辨率
	if ((Res_PreferredTim_temp.radio == RADIO_16_9) ||
	    (Res_PreferredTim_temp.radio == RADIO_16_10)) { //16:9分辨率
		//判断是否支持1280*720
		if (((Res_PreferredTim_temp.Haddressable == 1280) &&
		     (Res_PreferredTim_temp.Vaddressable == 720)) ||  //最佳时序判断是否为1280*720
		     Search_Standard_Tim(&Res_Standrad_Tim_temp_base1, 1280, 720) ||      //在标准时序中判断是否支持1280*720*60的分辨率
		     Search_Standard_Tim(&Res_Standrad_Tim_temp_base2, 1280, 720) ||      //在可能存在的标准时序中判断是否支持1280*720*60的分辨率
		     Search_ExtendTim_Tim(dattemp,&Res_extendTim, 1280, 720)) {           //如果存在扩展项则判断扩展项中是否支持1280*720*60的分辨率
			devDPUHDMI_SET_DEF(HDMI_1280_720_60, baseaddr, width, height, datatype, HDMI_CLOSE);//关闭显示
			devHDMI_CLK_CFG(HDMI_1280_720_60); // HDMI_1280_720   //时钟配置
			devDPUHDMI_SET_DEF(HDMI_1280_720_60, baseaddr, width, height, datatype, en_ctl);
			//devHDMI_RESET(); //复位操作
			sucess_flg = 1;      //当前显示器支持
		} else if ((established_tim_I_II[1] & 0x08) ||                                     //在內建时序中判断是否支持1024*768*60的分辨率
		//在不支持1280*720@60Hz的前提下,判断是否支持1024*768
		         Search_Standard_Tim(&Res_Standrad_Tim_temp_base1, 1024, 768) ||      //在标准时序中判断是否支持1024*768*60的分辨率
		         Search_Standard_Tim(&Res_Standrad_Tim_temp_base2, 1024, 768) ||      //在可能存在的标准时序中判断是否支持1024*768*60的分辨率
		         Search_ExtendTim_Tim(dattemp, &Res_extendTim, 1024, 768)) {          //如果存在扩展项则判断扩展项中是否支持1024*768*60的分辨率
			devDPUHDMI_SET_DEF(HDMI_1024_768_60,baseaddr,width,height,datatype,HDMI_CLOSE);   //关闭显示
			devHDMI_CLK_CFG(HDMI_1024_768_60); // HDMI_1280_720    //时钟配置
			devDPUHDMI_SET_DEF(HDMI_1024_768_60, baseaddr, width, height, datatype, en_ctl);   //设置为1024*768
			//devHDMI_RESET(); //复位操作
			sucess_flg = 1;      //当前显示器支持
		} else {
		//其他条件不支持
		//如果以上条件不支持，可提示不支持该屏幕或进行手动设置
			devDPUHDMI_SET_DEF(HDMI_1024_768_60, baseaddr, width, height, datatype, HDMI_CLOSE);   //设置为1024*768
			devHDMI_CLK_CFG(HDMI_1024_768_60); // HDMI_1024_768    //时钟配置
			//devHDMI_RESET(); //复位操作
			sucess_flg = 1;      //当前显示器支持
		}
	} else if ((Res_PreferredTim_temp.radio == RADIO_4_3) ||
	           (Res_PreferredTim_temp.radio == RADIO_5_4)) { //4:3 或者 5:4
	//决策判断4:3 和5:4的屏,将该比例的屏幕统一设置1024*768的图像
		//判断是否支持1024*768*60
		if (((Res_PreferredTim_temp.Haddressable == 1024) &&
		     (Res_PreferredTim_temp.Vaddressable == 768)) ||  //最佳时序判断是否为1024*768
		    ((established_tim_I_II[1] & 0x08)) ||                                //在內建时序中判断是否支持1024*768*60的分辨率
		    Search_Standard_Tim(&Res_Standrad_Tim_temp_base1, 1024, 768) ||      //在标准时序中判断是否支持1024*768*60的分辨率
		    Search_Standard_Tim(&Res_Standrad_Tim_temp_base2, 1024, 768) ||      //在可能存在的标准时序中判断是否支持1024*768*60的分辨率
		    Search_ExtendTim_Tim(dattemp, &Res_extendTim, 1024, 768)) {             //如果存在扩展项则判断扩展项中是否支持1024*768*60的分辨率
			devDPUHDMI_SET_DEF(HDMI_1024_768_60,baseaddr,width,height,datatype,HDMI_CLOSE);   //关闭显示
			devHDMI_CLK_CFG(HDMI_1024_768_60); // HDMI_1024_768   //时钟配置
			devDPUHDMI_SET_DEF(HDMI_1024_768_60,baseaddr,width,height,datatype,en_ctl);   //设置为1024*768
			//devHDMI_RESET(); //复位操作
			sucess_flg = 1;      //当前显示器支持
		} else if((Res_PreferredTim_temp.Haddressable == 1280 && Res_PreferredTim_temp.Vaddressable == 1024)||  //最佳时序判断是否为1280*1024*60
		//判断是否支持1280*1024*60
			Search_Standard_Tim(&Res_Standrad_Tim_temp_base1, 1280, 1024) ||      //在标准时序中判断是否支持1280*1024*60的分辨率
			Search_Standard_Tim(&Res_Standrad_Tim_temp_base2, 1280, 1024)||      //在可能存在的标准时序中判断是否支持1280*1024*60的分辨率
			Search_ExtendTim_Tim(dattemp,&Res_extendTim, 1280, 1024)) {            //如果存在扩展项则判断扩展项中是否支持1280*1024*60的分辨率
			devDPUHDMI_SET_DEF(HDMI_1280_1024_60, baseaddr, width, height, datatype, HDMI_CLOSE);   //关闭显示
			devHDMI_CLK_CFG(HDMI_1280_1024_60); // HDMI_1280_1024_60    //时钟配置
			devDPUHDMI_SET_DEF(HDMI_1280_1024_60, baseaddr, width, height, datatype, en_ctl);   //设置为HDMI_1280_1024_60
			//devHDMI_RESET(); //复位操作
			sucess_flg = 1;      //当前显示器支持
		} else {    //如果以上条件不支持，可提示不支持该屏幕或进行手动设置
			devDPUHDMI_SET_DEF(HDMI_1024_768_60, baseaddr, width, height, datatype, HDMI_CLOSE);
			devHDMI_CLK_CFG(HDMI_1024_768_60); // HDMI_1024_768    //时钟配置
			//devHDMI_RESET(); //复位操作
			sucess_flg = 0;  //当前显示器不支持
		}
	} else { //其余情况不支持，关闭其他纵横比不支持
		devDPUHDMI_SET_DEF(HDMI_1024_768_60, baseaddr, width, height, datatype, HDMI_CLOSE);
		devHDMI_CLK_CFG(HDMI_1024_768_60); // HDMI_1024_768     //时钟配置
		//devHDMI_RESET(); //复位操作
		sucess_flg = 0;  //当前显示器不支持
	}

	return sucess_flg;  //设置结果返回
}
