
#ifndef DEVDPUCONFIG_H_
#define DEVDPUCONFIG_H_

#define DPU_SCRN_WIDTH			1024
#define DPU_SCRN_HEIGHT			600


#define DPU_LAYER_WAVE_WIDTH		1000
#define DPU_LAYER_WAVE_HEIGHT		480

#define DPU_LAYER_TRAN_COLOR		0xc89860


#define DPU_LAYER_NUM			10
#define DPU_LAYER_CFG_NUM		5

//******************************************************************************
//******************************************************************************
#define DPU_REG_BASE			(0X43C00000)
#define DPU_REG_CFG_ADDR		(DPU_REG_BASE)

#define DPU_LCD_RST_REG			0
#define DPU_LCD_RST_MASK			0x00000001
#define DPU_WAVE_CFG_REG		1
#define DPU_WAVE_L1_MASK			0x00000007
#define DPU_WAVE_L2_MASK			0x00000038
#define DPU_WAVE_L3_MASK			0x000001C0
#define DPU_WAVE_L4_MASK			0x00000E00
#define DPU_WAVE_L5_MASK			0x00007000
#define DPU_WAVE_LX_OFF				3
#define DPU_WAVE_SIZE_REG		2
#define DPU_WAVE_WIDTH_MASK			0x000003FF
#define DPU_WAVE_CHX_REG		4
#define DPU_WAVE_PLT_CHX_MASK			0x00000070
#define DPU_WAVE_PLT_CHX_OFF			4
#define DPU_WAVE_PLT_CS_MASK			0x00008000
#define DPU_LAYER_POSXY_CS_MASK			0x00010000
#define DPU_WAVE_PLT_CHX_SET(CHX) \
	(((CHX) << DPU_WAVE_PLT_CHX_OFF) & DPU_WAVE_PLT_CHX_MASK)
#define DPU_WAVE_MODE_REG		5
#define DPU_WAVE_ENABLE_MASK			0x0000001F
#define DPU_TRACE_ENABLE_MASK			0x0000F000
#define DPU_TRACE_ENABLE_OFF			12
#define DPU_WAVE_ENABLE_SET(WAVE_CH ,TRACE_CH) \
	((((TRACE_CH) << DPU_TRACE_ENABLE_OFF) & DPU_TRACE_ENABLE_MASK) | \
	 ((WAVE_CH) & DPU_WAVE_ENABLE_MASK))
#define DPU_WAVE_TRANS_REG		6
#define DPU_WAVE_TRANS_MASK			0x000000FF
#define DPU_TRACE_COLOR_REG		8
#define DPU_TRACE_COLOR_MASK			0x00FFFFFF
#define DPU_TRACE_SIZE_REG		9
#define DPU_TRACE_WIDTH_MASK			0x000003FF
#define DPU_TRACE_TOP_MASK			0x00FF0000
#define DPU_TRACE_TOP_OFF			16
#define DPU_TRACE_SIZE_SET(W, T) \
	((((T) << DPU_TRACE_TOP_OFF) & DPU_TRACE_TOP_MASK) | \
	 ((W) & DPU_TRACE_WIDTH_MASK))
#define DPU_TRACE_CHX_REG		10
#define DPU_TRACE_CHX_MASK			0x0F000000
#define DPU_TRACE_CHX_OFF			24
#define DPU_TRACE_CHX_SET(CHX)			(((CHX) << DPU_TRACE_CHX_OFF) & DPU_TRACE_CHX_MASK)
#define DPU_TRACE_SCALE_REG		11
#define DPU_TRACE_SCALE_SEL_MASK		0x30000000
#define DPU_TRACE_SCALE_SEL_OFF			28
#define DPU_TRACE_SCALE_MASK			0x00000FFF
#define DPU_TRACE_SCALE_IN_MASK			0x00008000
#define DPU_TRACE_SCALE_IN_OFF			15
#define DPU_TRACE_OFFSET_MASK			0x03FF0000
#define DPU_TRACE_OFFSET_OFF			16
#define DPU_TRACE_SCALE_SET(CHX, IN, SCALE, OFFSET) \
	((((CHX) << DPU_TRACE_SCALE_SEL_OFF) & DPU_TRACE_SCALE_SEL_MASK) | \
	(((OFFSET) << DPU_TRACE_OFFSET_OFF) & DPU_TRACE_OFFSET_MASK) | \
	(((IN) << DPU_TRACE_SCALE_IN_OFF) & DPU_TRACE_SCALE_IN_MASK) | \
	((SCALE) & DPU_TRACE_SCALE_MASK))
#define DPU_LAYER_CFG_REG		12
#define DPU_LAYER_NUM_MASK			0x0000000F
#define DPU_SCRN_HEIGHT_MASK			0x0000FFC0
#define DPU_SCRN_HEIGHT_OFF			6
#define DPU_SCRN_WIDTH_MASK			0x07FF0000
#define DPU_SCRN_WIDTH_OFF			16
#define DPU_LAYER_CFG_SET(N, H, W) \
	((((W) << DPU_SCRN_WIDTH_OFF) & DPU_SCRN_WIDTH_MASK) | \
	(((H) << DPU_SCRN_HEIGHT_OFF) & DPU_SCRN_HEIGHT_MASK) | \
	((N) & DPU_LAYER_NUM_MASK))
#define DPU_LAYER_PAUSE_REG		14
#define DPU_LAYER_PAUSE_MASK			0x80000000
#define DPU_TEST_REG			15
#define DPU_TEST_MASK				0x0000FFFF

#define DPU_REG_CFG(OFF) \
	(DPU_REG_CFG_ADDR | ((OFF) << 2))

#define DPU_LAYER_CFG_ADDR		(DPU_REG_BASE + 0x800)
#define DPU_LAYER_CFG_INDEX			0x78
#define DPU_LAYER_CFG_INDEX_OFF			3

#define DPU_LAYER_CFG0_OFF		0
#define DPU_LAYER_COMB_MASK			0x00000001
#define DPU_LAYER_PRNT_MASK			0x00000002
#define DPU_LAYER_WRITE_MASK			0x0000001C
#define DPU_LAYER_WRITE_OFF			2
#define DPU_LAYER_EXTER_COMB			0x0
#define DPU_LAYER_INTER_COMB			0x1
#define DPU_LAYER_INTER_COMB_WR			0x2
#define DPU_LAYER_INTER_WR			0x3
#define DPU_LAYER_EXTER_WR			0x4
#define DPU_LAYER_PRINT_WR			0x5
#define DPU_LAYER_MODE_MASK			0x00000060
#define DPU_LAYER_MODE_OFF			5
#define DPU_LAYER_OVERLAY			0x0
#define DPU_LAYER_BOTH				0x1
#define DPU_LAYER_ALPHA				0x2
#define DPU_LAYER_TYPE_MASK			0x00000380
#define DPU_LAYER_TYPE_OFF			7
#define DPU_LAYER_PIC				0x0
#define DPU_LAYER_FILLE				0x1
#define DPU_LAYER_WAVE				0x2
#define DPU_LAYER_TRACE				0x3
#define DPU_LAYER_MASK				0x4
#define DPU_LAYER_RULER				0x5
#define DPU_LAYER_CURSOR			0x6
#define DPU_LAYER_COMB				0x7
#define DPU_LAYER_HEIGHT_MASK			0x001FF800
#define DPU_LAYER_HEIGHT_OFF			11
#define DPU_LAYER_WIDTH_MASK			0xFFE00000
#define DPU_LAYER_WIDTH_OFF			21

#define DPU_LAYER_CFG1_OFF		1
#define DPU_LAYER_FRAME_MASK			0x0000000F
#define DPU_LAYER_COLOR_MASK			0x00000070
#define DPU_LAYER_COLOR_OFF			4
#define DPU_LAYER_NOPIC				0x0
#define DPU_LAYER_R8G8B8			0x0
#define DPU_LAYER_R8G8B81			0x1
#define DPU_LAYER_R5G6B5			0x2
#define DPU_LAYER_INDEX8			0x3
#define DPU_LAYER_REVERSE_MASK			0x00000080
#define DPU_LAYER_REVERSE_OFF			7
#define DPU_LAYER_FLIPX_MASK			0x00000100
#define DPU_LAYER_FLIPX_OFF			8
#define DPU_LAYER_FLIPY_MASK			0x00000200
#define DPU_LAYER_FLIPY_OFF			9
#define DPU_LAYER_POSY_MASK			0x001FF800
#define DPU_LAYER_POSY_OFF			11
#define DPU_LAYER_POSX_MASK			0xFFE00000
#define DPU_LAYER_POSX_OFF			21

#define DPU_LAYER_CFG2_OFF		2
#define DPU_LAYER_ALPHA_MASK			0x000000FF
#define DPU_LAYER_ALPHA_OFF			0
#define DPU_LAYER_TCOLOR_MASK			0xFFFFFF00
#define DPU_LAYER_TCOLOR_OFF			8

#define DPU_LAYER_CFG3_OFF		3
#define DPU_LAYER_ADDR_MASK			0x3FFFFFFF

#define DPU_LAYER_CFG4_OFF		4
#define DPU_LAYER_FRESET_MASK			0x00000001
#define DPU_LAYER_ARESET_MASK			0x00000002
#define DPU_LAYER_RRESET_MASK			0x00000004

#define DPU_LAYER_FAUTO_MASK			0x00000030
#define DPU_LAYER_FAUTO_OFF			4
#define DPU_LAYER_FDIRECT_MASK			0x00000040
#define DPU_LAYER_FDIRECT_OFF			6
#define DPU_LAYER_FONCE_MASK			0x00000080
#define DPU_LAYER_FONCE_OFF			7
#define DPU_LAYER_FRAMES_MASK			0x00000F00
#define DPU_LAYER_FRAMES_OFF			8
#define DPU_LAYER_FTIME_MASK			0x0000F000
#define DPU_LAYER_FTIME_OFF			12

#define DPU_LAYER_AAUTO_MASK			0x00030000
#define DPU_LAYER_AAUTO_OFF			16
#define DPU_LAYER_ASTEP_MASK			0x03FC000
#define DPU_LAYER_ASTEP_OFF			18
#define DPU_LAYER_ATIME_MASK			0x3C000000
#define DPU_LAYER_ATIME_OFF			26

#define DPU_LAYER_RAUTO_MASK			0xC0000000
#define DPU_LAYER_RAUTO_OFF			30
#define DPU_LAYER_AUTO_REG			0x0
#define DPU_LAYER_AUTO_EXT			0x2
#define DPU_LAYER_AUTO_INT			0x3

#define DPU_LAYER_CFG5_OFF		5
#define DPU_LAYER_XYRESET_MASK			0x00000001
#define DPU_LAYER_XYONCE_MASK			0x00000002
#define DPU_LAYER_XYAUTO_MASK			0x00000004
#define DPU_LAYER_YROLLABLE_MASK		0x00000008
#define DPU_LAYER_XROLLABLE_MASK		0x00000010
#define DPU_LAYER_XSTEP_MASK			0x000000E0
#define DPU_LAYER_XSTEP_OFF			5
#define DPU_LAYER_YSTEP_MASK			0x00000700
#define DPU_LAYER_YSTEP_OFF			8
#define DPU_LAYER_VIEWY_MASK			0x001FF800
#define DPU_LAYER_VIEWY_OFF			11
#define DPU_LAYER_VIEWX_MASK			0xFFE00000
#define DPU_LAYER_VIEWX_OFF			21

#define DPU_VERSION_REG			(DPU_REG_CFG_ADDR + (15 << 2))
#define DPU_RUN_STATUS_REG		(DPU_REG_CFG_ADDR + (14 << 2))

#define DPU_LAYER_CFG(INDEX, OFF) \
	(DPU_LAYER_CFG_ADDR | \
	 (((((INDEX) << DPU_LAYER_CFG_INDEX_OFF) & DPU_LAYER_CFG_INDEX) | (OFF)) << 2))


#define DPU_LAYER_PLT_ADDR		(DPU_REG_BASE + 0x800)
#define DPU_LAYER_PLT_INDEX			0xFF
#define DPU_LAYER_PLT(INDEX) \
	(DPU_LAYER_PLT_ADDR | (((INDEX) & DPU_LAYER_PLT_INDEX) << 2))

#define DPU_WAVE_PLT_ADDR		(DPU_REG_BASE + 0x800)
#define DPU_WAVE_PLT_INDEX			0xFF
#define DPU_WAVE_PLT(INDEX)			(DPU_WAVE_PLT_ADDR | ((INDEX & DPU_WAVE_PLT_INDEX) << 2))

#define DPU_LAYER_POS_ADDR		(DPU_REG_BASE + 0xc00)
#define DPU_LAYER_YPOS_MASK			0x003F
#define DPU_LAYER_XPOS_MASK			0xFFC0
#define DPU_LAYER_POSXY_SET(POSX, POSY) \
	((((POSX) << 6) & DPU_LAYER_XPOS_MASK) | \
	 ((POSY) & DPU_LAYER_YPOS_MASK))

#define DPU_TRACE_DATA_ADDR		(DPU_REG_BASE + 0x1000)
#define DPU_TRACE_DATA_INDEX			0x3FF
#define DPU_TRACE_DATA(INDEX) \
	(DPU_TRACE_DATA_ADDR | (((INDEX) & DPU_TRACE_DATA_ADDR) << 2))

#define DPU_TRACE_DATA_NULL		0x0

typedef struct {
	//0
	bool            bLayerCombinable        : 1;
	bool            bLayerPrintable         : 1;
	unsigned long   u3LayerWriteMode        : 3;
	unsigned long   u2LayerCombMode         : 2;
	unsigned long   u3LayerType             : 3;
	unsigned long   uxLayerReserved0        : 1;
	unsigned long   u9LayerHeight           :10;
	unsigned long   u10LayerWidth           :11;
	//1
	unsigned long   u4LayerFrame            : 4;
	unsigned long   u3LayerColorType        : 3;
	bool            uxLayerReserved1        : 1;
	bool            bLayerFlipx             : 1;
	bool            bLayerFlipy             : 1;
	unsigned long   bLayerReverse           : 1;
	unsigned long   u9LayerPosy             :10;
	unsigned long   u10LayerPosx            :11;
	//2
	unsigned long   u8LayerAlpha            : 8;
	unsigned long   u24LayerTransColor      :24;
	//3
	unsigned long   u32LayerBaseaddr;
	//4
	bool            bLayerFrameReset        : 1;
	bool            bLayerAlphaReset        : 1;
	bool            bLayerReverseReset      : 1;
	bool            bLayerClearSet          : 1;

	unsigned long   u2LayerFrameAuto        : 2;
	bool            bLayerFrameDirect       : 1;
	bool            bLayerFrameOnce         : 1;
	unsigned long   u4LayerFrameNum         : 4;
	unsigned long   u4LayerFrameTime        : 4;

	unsigned long   u2LayerAlphaAuto        : 2;
	long            s8LayerAlphaStep        : 8;
	unsigned long   u4LayerAlphaTime        : 4;

	unsigned long   u2LayerReverseAuto      : 2;
	//5
	bool            bLayerViewReset         : 1;
	bool            bLayerViewYAuto         : 1;
	bool            bLayerViewXAuto         : 1;
	bool            bLayerRollableY         : 1;
	bool            bLayerRollableX         : 1;
	unsigned long   u3LayerViewYStep        : 3;
	unsigned long   u3LayerViewXStep        : 3;
	unsigned long   u9LayerViewY            :10;
	unsigned long   u10LayerViewX           :11;
} ST_LAYER_CFG;


typedef struct {
	bool            bTraceIn;
	unsigned short  u16TraceScale;
	short           s16TraceOffset;
} ST_TRACE_SCALE;

typedef struct {
	unsigned char   u8LayerNumber;
	int             s32screenWidth;
	int             s32screenHeight;

	int             s32VectorEn;
	int             s32WaveOrder;

	bool            bWaveChxEnable[5];
	bool            bTraceChxEnable[4];
	unsigned char   u8WaveLayerChx[5];
	unsigned short  u16WaveLayerWidth;
	unsigned char   u8WaveLayerTransData;

	unsigned long   u32TraceLayerColor;
	unsigned short  u16TraceLayerWidth;
	unsigned char   u8TraceLayerTop;
	ST_TRACE_SCALE  stTraceScale[4];

	ST_LAYER_CFG    stLayerConfig[DPU_LAYER_NUM];

	unsigned long   *pu32LayerPaletteData;
	unsigned long   *pu32WavePaletteData;
	unsigned char   *pu8TraceData;
} ST_DPU_CFG;

typedef struct {
    u16                 u6LayerRow              : 6;
    u16                 u10ScreenCol            :10;
} ST_POSXY;

void devDPURegSet(unsigned long *pu32RegAddr,        // ŒÄŽæÆ÷µØÖ·
                  unsigned long  u32RegData);        // ŒÄŽæÆ÷ÊýŸÝ
#endif /* DEVDPUCONFIG_H_ */
